<?php
class M_member extends CI_Model
{

    function get_member() //tampil data member
    {
        $result = $this->db->get('member');
        return $result;
    }

    function get_member_email($email = NULL) {
        if($email == NULL) {
            return false;
        } else {
           return $this->db
           ->get_where('member', array('email' => $email))
           ->row(); 
        }
    }

    function get_token($token = NULL) {
        if($token == NULL) {
            return false;
        } else {
           return $this->db
           ->get_where('member', array('token' => $token))
           ->row(); 
        }
    }

    function member_with_token($email = NULL, $token = NULL) {
        if($token == NULL && $email == NULL) {
            return false;
        } else {
           return $this->db
           ->get_where('member', array(
               'token' => $token,
               'email' => $email
            ))
           ->row(); 
        }
    }

    
    function update_password($email = NULL, $token = NULL, $password = array()) {
        if($token == NULL && $email == NULL) {
            return false;
        } else {
            return  $this->db
            ->where(array(
                'email' => $email,
                'token' => $token
            ))
            ->update('member', $password);
        }
    }

    function update_data($email = NULL, $token = NULL, $payload = array()) {
        if($token == NULL && $email == NULL) {
            return false;
        } else {
            return  $this->db
            ->where(array(
                'email' => $email,
                'token' => $token
            ))
            ->update('member', $payload);
        }
    }

    function update_data_member($where, $data, $table) // Update data produk
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }



}
