<?php
class M_Stok extends CI_Model
{
    // function cari() 
    // {
        
        // $this->db->select('*');

        // $this->db->from('produk');
        // $this->db->join('stok', 'produk.id_produk=stok.id_produk');
      
        // return $this->db->get();
        // $this->db->like('nama_produk' $katakunci);

        // return $this->db->get();
    // }
    function get_datastok() 
    {
        $result = $this->db->select('produk.nama_produk, stok. *,kategori.nama_kategori ')-> 
            join('produk', 'produk.id_produk = stok.id_produk')->
            join('kategori', 'kategori.id_kategori = produk.id_kategori')->
            get('stok');
        return $result;
    }

    function get_datastok_where($payload = array()) 
    {
        $result = $this->db->select('produk.nama_produk, stok. *,kategori.nama_kategori ')-> 
            join('produk', 'produk.id_produk = stok.id_produk')->
            join('kategori', 'kategori.id_kategori = produk.id_kategori')->
            where($payload)->
            get('stok');
        return $result;
    }

    public function getOneStokByIdProduk($id_produk){
        return $this->db->select('*')->where('id_produk', $id_produk)->get('stok')->result();
    }

    function get_kategori() //tampil data kategori
    {
        $result = $this->db->get('stok');
        return $result;
    }

    function input_datas($data, $table) // Tambah data kategori
    {
        $this->db->insert($table, $data);
        // $this->db->where($id_produk);
    }


    public function input_data($id_produk){
        $this->db->from('stok');
        $this->db->where($id_produk);
        $this->db->where('ukuran_stok', $this->input->post('ukuran_stok'));
        if($this->db->get()->result()){
            return false;
        }
    
        $data = [
            'id_produk' => $id_produk,
            'ukuran_stok' => $this->input->post('ukuran_stok'),
            'jumlah_stok' => $this->input->post('jumlah_stok')
        ];
        $this->db->insert('stok',$data);
    }

}

?>
