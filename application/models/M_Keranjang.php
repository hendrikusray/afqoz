<?php
class M_Keranjang extends CI_Model
{

    function get_keranjang($idmember = NULL) //tampil data member
    {
        $result = $this->db->select('*,cart.harga AS harga_stok')
            ->from('cart')
            ->join('produk', 'cart.id_produk = produk.id_produk')
            ->where(array(
                'id_member' => $idmember,
            ))
            ->get()
            ->result_array();
        // var_dump($this->db->last_query());
        return $result;
    }

    function get_keranjang_all($condition = array()) //tampil data member
    {
        $result = $this->db->select('*, cart.harga AS harga_stok')
            ->from('cart')
            ->join('produk', 'cart.id_produk = produk.id_produk')
            ->where($condition)
            ->get()
            ->result_array();
        // var_dump($this->db->last_query());
        return $result;
    }

    function count_selected($idmember = NULL)
    {

        $result = $this->db->select('SUM(total_harga) AS total')
            ->select('COUNT(id) as total_produk')
            ->select('SUM(qty) as total_cart')
            ->from('cart')
            ->where(array(
                'id_member' => $idmember,
                'is_selected' => 1
            ))
            ->get()
            ->row();
        // var_dump($this->db->last_query());
        return $result;
    }

    function count($idmember = NULL)
    {

        $result = $this->db->select('SUM(total_harga) AS total')
            ->select('COUNT(id) as total_produk')
            ->select('SUM(qty) as total_cart')
            ->from('cart')
            ->where(array(
                'id_member' => $idmember,
            ))
            ->get()
            ->row();
        // var_dump($this->db->last_query());
        return $result;
    }

    function addtoCart($payload = array())
    {
        $this->db->insert('cart', $payload);
    }

    function find($condition = array()) //tambahan
    {
        $result = $this->db->where($condition)
            ->limit(1)
            ->get('cart')
            ->row();

        return $result;
    }

    function delete($condition = array(),$table = 'cart') //tambahan
    {
        $result = $this->db->delete($table, $condition); 

        return $result;
    }

    
    function update_data($where, $data, $table) // Update data produk
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
}
