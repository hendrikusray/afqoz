<?php
class M_laporan extends CI_Model
{

    function get_laporan() //tampil data member
    {
        $result = $this->db->get('produk');
        return $result;
    }

    function get_member() //tampil data member
    {
        $result = $this->db->select('*')
            ->from('member')
            ->get()
            ->result();
        return $result;
    }

    function get_member_condition($payload = array()) //tampil data member
    {
        $result = $this->db->select('*')
            ->from('member')
            ->where($payload)
            ->get()
            ->result();
        return $result;
    }


}
