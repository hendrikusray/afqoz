<?php
class M_transaction extends CI_Model
{

    function get_all_transactions($condition = array()) //tampil barang ke halaman
    {
        $result = $this->db
            ->select('*')
            ->where($condition)
            ->get('pembayaran')
            ->result();
            for ($i=0; $i <count($result) ; $i++) { 
                $result[$i]->produk    = $this->produk_relation(array('pemesanan.id_pembayaran' =>  $result[$i]->id));
                $result[$i]->all_total = $this->all_total_transaction(array('pemesanan.id_pembayaran' =>  $result[$i]->id));
            }   
            // var_dump($result);
        return $result;
    }

    function produk_relation($condition = array()) //tampil barang ke halaman
    {
        $result = $this->db
            ->select('produk.*,pemesanan.qty,pemesanan.harga,pemesanan.ukuran,pemesanan.nama_produk')
            ->from('pemesanan')
            ->join('order_produk', 'order_produk.id_order = pemesanan.id_pemesanan')
            ->join('produk', 'order_produk.id_produk = produk.id_produk')
            ->where($condition)
            ->get()
            ->result();
        // $result= json_decode($result['foto_produk']);
        return $result;
    }

    function report_transaction() //tampil barang ke halaman
    {

        $result = $this->db->select('*,pembayaran.provinsi AS province,pembayaran.kabupaten AS district')
            ->from('pembayaran')
            ->join('member', 'pembayaran.id_member = member.id_member')
            ->get()
            ->result();

            for ($i=0; $i <count($result) ; $i++) { 
                $result[$i]->total_pesanan = $this->count_pesanan(array('id_member' =>  $result[$i]->id_member, 'id' =>$result[$i]->id ));
                $result[$i]->all_total     = $this->all_total_transaction(array('pemesanan.id_pembayaran' =>  $result[$i]->id));
            }   
        return $result;
    }

    
    function report_transaction_function($condition = array()) //tampil barang ke halaman
    {

        $result = $this->db->select('*,pembayaran.provinsi AS province,pembayaran.kabupaten AS district')
            ->from('pembayaran')
            ->where($condition)
            ->join('member', 'pembayaran.id_member = member.id_member')
            ->get()
            ->result();
        
            for ($i=0; $i <count($result) ; $i++) { 
                $result[$i]->total_pesanan = $this->count_pesanan(array('id_member' =>  $result[$i]->id_member, 'id' =>$result[$i]->id ));
                 $result[$i]->all_total     = $this->all_total_transaction(array('pemesanan.id_pembayaran' =>  $result[$i]->id));
            }   
        return $result;
    }

    function count_pesanan($condition = array()) //tampil barang ke halaman
    {
        $result = $this->db
            ->select('SUM(pemesanan.qty) as total_pemesanan')
            ->from('pembayaran')
            ->join('pemesanan', 'pembayaran.id = pemesanan.id_pembayaran')
            ->where($condition)
            ->group_by('pemesanan.id_pembayaran')
            ->get()
            ->row();
        $result = $result->total_pemesanan;
        return $result;
    }

    function count_grafik($condition = array()) //tampil barang ke halaman
    {
        $result = $this->db
            ->select('SUM(pemesanan.qty) as total_pemesanan')
            ->from('pembayaran')
            ->join('pemesanan', 'pembayaran.id = pemesanan.id_pembayaran')
            ->where($condition)
            ->group_by('pemesanan.id_pembayaran')
            ->get()
            ->row();
        if($result !== NULL) {
            $result = $result->total_pemesanan;
        }
        
        return $result;
    }

    function all_total_transaction($condition = array()) //tampil barang ke halaman
    {
        $result = $this->db
            ->select('Sum(harga) as Harga, Sum(qty) as Qty, Sum(Harga*Qty) as total_transaction')
            ->from('pemesanan')
            ->where($condition)
            ->get()
            ->row();
        if($result !== NULL) {
            $result = $result->total_transaction;
        }
        
        return $result;
    }

    function delete_data($where, $table) //Hapus data kategori
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function update_data($where, $data, $table) // Update data artikel
    { 
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function payment($payload = array(), $table = '')
    {
        $this->db->insert($table, $payload);
        $id = $this->db->insert_id();
        return $id;
    }

    // function getPemesanan($condition = array()) {
    //     $result = $this->db
    //     ->select('*,stok.harga AS harga_stok')
    //     ->where($condition)
    //     ->get('p')
    //     ->row();

    //     return $result;
    // }

}
