<?php
class M_produk extends CI_Model
{

    function get_produk() //tampil barang ke halaman
    {
        // $result= $this->db->get('produk');
        // return $result;
        $result = $this->db->select('*')
                  ->from('produk')
                  ->where('status', false)
                  ->join('kategori', 'kategori.id_kategori = produk.id_kategori')
                  ->get()
                  ->result();
        // var_dump($this->db->last_query());
        return $result;
    }

    function getproductbykategory($kategori) {
        $result = $this->db->select('*')
        ->from('produk')
        ->where(array(
            'kategori.id_kategori' => $kategori
        ))
        ->join('kategori', 'kategori.id_kategori = produk.id_kategori')
        ->get()
        ->result();

        return $result;
    }

    function get_kategori() //tampil kategori ke halaman
    {
        $this->db->select('*');
        return $this->db->get('kategori');
    }

    function get_detil_produk($id_produk) //tampil detil produk pada halaman member
    {
        $detil = $this->db->from('produk')->where('id_produk', $id_produk)->where('status', false)->get()->result();
        foreach ($detil as $key => $value) {
            $detil[$key]->stok = $this->db->select('*')->where('id_produk', $value->id_produk)->get('stok')->result();
        }
        if (count($detil) > 0) {
            return $detil;
        } else {
            return false;
        }
        // $this->db->order_by('id_produk');
        // $result = $this->db->get('produk',6,0);
        // return $detil;
    }

    function get_produk_page() //tampil random produk page
    {
        $this->db->order_by('id_produk', 'DESC');
        $this->db->where('status', false);
        $result = $this->db->get('produk');
        return $result;
    }


    function input_data($data, $table) // Tambah data produk
    {
        $this->db->insert($table, $data);
    }

    function delete_data($where, $table) //Hapus data produk
    {
        $this->db->where($where);
        $this->db->update('produk', ['status' => true]);
    }


    function update_data($where, $data, $table) // Update data produk
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    function tambahSize($id_produk)
    {
        $data   = false;
        $result = $this->db
            ->select('*')
            ->from('stok')
            ->where(array(
                'stok.id_produk' => $id_produk,
                'stok.ukuran_stok' => $this->input->post('ukuran', true)
            ))
            ->get()
            ->result();
           
        if(empty($result)) {
            $data = [
                "id_produk"   =>  $id_produk,
                "ukuran_stok" => strtoupper($this->input->post('ukuran', true)), //biasanya $_POST["nama"]
                "jumlah_stok" => $this->input->post('jml', true), 
                'harga' => $this->input->post('harga', true)
            ];
            $this->db->insert('stok', $data);
            return true;
        } else if (!empty($result)) {
            if(empty($this->input->post('harga', true))) {
                $result = $this->db
                ->where(array(
                    'stok.id_produk' => $id_produk,
                    'stok.ukuran_stok' => $this->input->post('ukuran', true),
                ))
                ->update('stok',  array(
                    "jumlah_stok" => strtoupper($this->input->post('jml', true)),
                ));
            } else {
                $result = $this->db
                ->where(array(
                    'stok.id_produk' => $id_produk,
                    'stok.ukuran_stok' => $this->input->post('ukuran', true),
                ))
                ->update('stok',  array(
                    "jumlah_stok" => strtoupper($this->input->post('jml', true)),
                    'harga' => $this->input->post('harga', true)
                ));
            }
                // die();
            return true;
        } else {
            return false;
        }
        // $this->db->from('stok');
        // $this->db->where($id_produk);
        // $this->db->where('ukuran_stok', $this->input->post('ukuran', true));
        // $this->db->where('jumlah_stok', $this->input->post('jml', true));
        // if($this->db->get()->result()){ 
        //     return false;
        // }


        // return true;
    }

    // function get_datastok() 
    // {
    //     $result = $this->db->select('produk.nama_produk, stok. * ')-> 
    //         join('produk', 'produk.id_produk = stok.id_produk','right')->
    //         get('stok');
    //     return $result;
    // }


    function find($id_produk) //tambahan
    {
        $result = $this->db
            ->select('*,stok.harga AS harga_stok')
            ->where('produk.id_produk', $id_produk)
            ->join('stok', 'produk.id_produk = stok.id_produk')
            ->limit(1)
            ->get('produk')
            ->row();

            return $result;
    }

    
    function find_uk($condition = array()) //tambahan
    {
        $result = $this->db
            ->select('*,stok.harga AS harga_stok')
            ->where($condition)
            ->join('stok', 'produk.id_produk = stok.id_produk')
            ->limit(1)
            ->get('produk')
            ->row();

            return $result;
    }

    public function show($id_produk)
    {
        return $this->db->where('id_produk', $id_produk)->get('produk')->result();
    }

    public function withStok($search)
    {   
        $produk = array();
        if($search === NULL) {
            $produk = $this->db
            ->select('*')
            ->where(array(
                'status' => 0,
            ))
            ->get('produk')
            ->result();
        } else {
            $produk = $this->db
            ->select('*')
            ->where(array(
                'status' => 0,
            ))
            ->like('nama_produk', $search)
            ->get('produk')
            ->result();
        }
        

        foreach ($produk as $key => $value) {
            $produk[$key]->stok = $this->db->select('*')->where('id_produk', $value->id_produk)->get('stok')->result();
        }

        return $produk;
    }
}
