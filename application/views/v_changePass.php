<div class="row justify-content-center">
  <div class="col-lg-5">
    <div class="card o-hidden border-1 ">
      <div class="card-body p-0">
        <div class="row">
          <div class="col-lg">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Change Your Password </h1>
              </div>
              <form action="<?php echo base_url() . 'member/akunku/changePass' ?>" method="post">
                <div class="form-group">
                  <input required type="password" class="form-control" id="update_password" name="update_password" placeholder="Enter New Password">
                </div>
                <div class="form-group">
                  <input required type="password" class="form-control" id="repeat_update_password" name="repeat_update_password" placeholder="Repeat Password">
                  <div style="margin-top: 8px;">
                    <span id='message'></span>
                  </div>
                </div>
                <div class="form-group">
                  <button style="color: white;" id="submit_update" type="submit" class="btn btn-primary btn-block p-2">
                    Change Password
                  </button>
                  <div class="text-center">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('#update_password, #repeat_update_password').on('keyup', function() {
    if ($('#update_password').val() == '') {
      $('#message').css('visibility', 'hidden');
    } else if ($('#update_password').val() == $('#repeat_update_password').val()) {

      $("#submit_update").prop('disabled', false);
      console.log($("#submit_update").prop('disabled', false))
      $('#message').html(`<div class="alert alert-success" role="alert">password match</div>`).css({
        'color': 'green',
        'visibility': 'visible'
      });
    } else {
      $('#message').html('<div class="alert alert-danger" role="alert">password not match</div>').css({
        'color': 'red',
        'visibility': 'visible'
      });
      $("#submit_update").prop('disabled', true);
    }
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous" />
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous" />