<div class="container mt-4">
    <div class="card w-75">
        <div class="card-body">
            <h5 class="card-title">Ubah Password</h5>
            <hr>

            <div class="col-lg-6">
            </div>
            <div class="col-md-2"></div>
            <!-- <p class="card-text"> -->
            <form action="<?php echo base_url() . 'forgotpass/changePassword' ?>" method="post">
                <div class="form-group">
                    <label for="current_password">Email</label>
                    <input readonly type="text"  value="<?php echo $email; ?>" class="form-control" id="email_change_pw" name="email_change_pw">
                    <input type="hidden" name="token" value="<?php echo $token; ?>" style="display: none">
                </div>

                <div class="form-group" >
                    <label for="current_password">Password</label>
                    <input type="password" required class="form-control" id="new_password" name="new_password">

                </div>
                <div class="form-group">
                    <button type="submit" style="color: white;" class="btn btn-primary">Ubah Password</button>
                </div>
            </form>
        </div>
    </div>
</div>