<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <title>Email Template</title>
  </head>
  <body style="margin: 0; background: #edeff4; text-decoration: none">
    <div style="width: 100%; margin: auto">
      <div
        style="
          height: 94px;
          width: 100%;
          background: linear-gradient(90.04deg, #f48a00 0.04%, #ed7642 65.51%);
        "
      ></div>
      <div
        style="
          max-width: 724px;
          display: flex;
          flex-wrap: wrap;
          flex-direction: column;
          margin: 0 auto;
          position: relative;
          top: -116px;
        "
      >
        <div
          style="
            max-width: 1000px;
            width: 135%;
            height: 800px;
            margin: 0 auto;
            display: flex;
            overflow: hidden;
            border-radius: 4px;
            background : #fff
          "
        >
          <div
            style="
              height: 0;
              margin: -36 22;
              background: #fff;
              flex: 1 1 0%;
            "
          >
            <div>
              <img
                src="https://i.imgur.com/KrEd2L2_d.webp?maxwidth=760&fidelity=grand"
                alt=""
                style="
                  margin-top: 38px;
                  display: block;
                  margin-left: auto;
                  margin-right: auto;
                  width: 30%;
                "
              />
            </div>
            <div
              style="
                margin-top: 38px;
                border: 1px solid #edeff4;
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 90%;
              "
            ></div>
            <div style="padding: 20px 60px">
              <h3 
                style="
                  font-family: Nunito Sans;
                  font-style: normal;
                  font-weight: 900;
                  font-size: 22px;
                  line-height: 120%;
                  color: #172029;
                  text-align: center;
                "
              >
               Aktifkan Akun Anda
              </h3>
              <p
                style="
                  font-family: Nunito Sans;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 16px;
                  line-height: 180%;
                  text-align: center;
                  color: #7d7d7d;
                  mix-blend-mode: normal;
                  opacity: 0.8;
                "
              >
               Selamat Datang di AFQOZ. silahkan klik tombol aktifasi agar akun anda dapat belanja,
               terimakasih
              </p>
              <center>
                <a href="<?php echo base_url() . 'register/aktifasiAkun?q=' . $token ?>"
                  style="
                    border: none;
                    border-radius: 4px;
                    color: white;
                    padding: 15px 32px;
                    text-align: center;
                    text-decoration: none;
                    display: inline-block;
                    font-size: 16px;
                    margin: 4px 2px;
                    cursor: pointer;
                    background-color: #ba0202;
                    margin-top: 44px;
                  "
                >
                 Aktivasi Akun
                </a>
              </center>
              <div
                style="
                  margin-top: 38px;
                  border: 1px solid #edeff8;
                  display: block;
                  margin-left: auto;
                  margin-right: auto;
                  width: 90%;
                "
              ></div>
              <p
                style="
                  font-family: Nunito Sans;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 16px;
                  line-height: 180%;
                  text-align: center;
                  color: #7d7d7d;
                  mix-blend-mode: normal;
                  opacity: 0.8;
                "
              >
                jika tombol tidak bisa ditekan,
                <a href="<?php echo base_url() . 'register/aktifasiAkun?q=' . $token ?>"
                  style="
                    font-family: Nunito Sans;
                    font-style: normal;
                    font-weight: bold;
                    font-size: 14px;
                    line-height: 160%;
                    text-align: center;
                    color: #ba0202;
                    mix-blend-mode: normal;
                    opacity: 0.8;
                    cursor: pointer;
                  "
                  >klik disini</a
                >
               silahkan klik link berikut.
               
              </p>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <footer></footer>
  </body>
</html>
