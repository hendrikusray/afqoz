<div class="page-content-wrapper ">
	<div class="container-fluid">

	<h2>Data Transaksi</h2><hr>
    <form method="get" action="">
        <label><b>Filter Berdasarkan</b></label><br>
        <select name="filter" id="filter">
            <option value="">Pilih</option>
            <option value="1">Per Tanggal</option>
            <option value="2">Per Bulan</option>
            <option value="3">Per Tahun</option>
        </select>
        <br /><br />

        <div id="form-tanggal">
            <label>Tanggal</label><br>
            <input type="text" name="tanggal" class="input-tanggal" />
            <br /><br />
        </div>

        <div id="form-bulan">
            <label>Bulan</label><br>
            <select name="bulan">
                <option value="">Pilih</option>
                <option value="1">Januari</option>
                <option value="2">Februari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
            </select>
            <br /><br />
        </div>

        <div id="form-tahun">
            <label>Tahun</label><br>
            <select name="tahun">
                <option value="">Pilih</option>
                <option value="">2020</option>
                <option value="">2019</option>
            </select>
            <br /><br />
        </div>

        <button type="submit">Tampilkan</button>
        <a href="index.php">Reset Filter</a>
        
    </form>
    <hr />

		<div class="row">
			<div class="col-sm-12">
				<div class="float-right page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">KPTI</a></li>
						<li class="breadcrumb-item"><a href="#">Rangking</a></li>
						<li class="breadcrumb-item active">Data & Hasil Perangkingan</li>
					</ol>
				</div>
				<h5 class="page-title">Data & Hasil Perangkingan</h5>
			</div>
		</div>
		<!-- end row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-6">
				<div class="card m-b-30">
					<div class="card-body">
						<div class="tab-pane p-3" id="profile" role="tabpanel">
							<h4 class="mt-0 header-title">Laporan Laba</h4>
							<p class="text-muted m-b-30 font-14">The Buttons extension for
								DataTables
								provides a common set of options, API methods and styling to
								display
								buttons on a page that will interact with a DataTable. The
								core library
								provides the based framework upon which plug-ins can built.
							</p>

							<table id="datatable-buttons"
								class="table table-striped table-bordered dt-responsive nowrap"
								style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
									<tr>
										<th>Name</th>
										<th>Position</th>
										<th>Office</th>
										<th>Age</th>
										<th>Start date</th>
										<th>Salary</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>Tiger Nixon</td>
										<td>System Architect</td>
										<td>Edinburgh</td>
										<td>61</td>
										<td>2011/04/25</td>
										<td>$320,800</td>
									</tr>
									<tr>
										<td>Donna Snider</td>
										<td>Customer Support</td>
										<td>New York</td>
										<td>27</td>
										<td>2011/01/25</td>
										<td>$112,000</td>
									</tr>
								</tbody>
							</table>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- end row -->
</div><!-- container fluid -->
</div> <!-- Page content Wrapper -->
