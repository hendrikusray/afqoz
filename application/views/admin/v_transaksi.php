<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Afqoz</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active">Transaksi</li>
                    </ol>
                </div>
                <h5 class="page-title">Data Transaksi</h5>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Semua data transaksi</h4>
                        <!-- <div class="d-flex justify-content-end"><button type="submit" class="btn btn-primary">download</button></div> -->
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered dt-responsive  text-justify" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID-Pemesanan</th>
                                        <th class="text-center">Nama Member</th>
                                        <th class="text-center">Jumlah Pemasanan</th>
                                        <th class="text-center">Total Pemesanan</th>
                                        <th class="text-center">status pemesanan</th>
                                        <th class="text-center">alamat</th>
                                        <th class="text-center">action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($data as $red) :
                                        $codeorder      = $red->ordercode;
                                        $nama_member    = $red->nama_member;
                                        $total_harga    = $red->all_total;
                                        $status         = $red->status_pemesanan;
                                        $id             = $red->id;
                                        $total          = $red->total_pesanan;
                                        $status         = $red->status_barang;
                                        $alamat         = $red->alamat . ', ' . $red->district . ', ' . $red->province;
                                        if ($status === 'settlement') {
                                            $status = 'sedang di kemas';
                                        }
                                    ?>

                                        <tr>
                                            <th scope="row" class="text-left">order-<?php echo $codeorder; ?></th>
                                            <td style="text-align: initial;"><?php echo $nama_member; ?></td>
                                            <td><?php echo $total; ?></td>
                                            <td>Rp.<?php echo $total_harga; ?></td>
                                            <td><?php echo $status; ?></td>
                                            <td style="text-align: initial;"><?php echo $alamat; ?></td>
                                            <td>
                                                <div class="d-flex flex-row bd-highlight mb-3">
                                                    <div class="p-1 bd-highlight">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $id; ?>">
                                                            edit
                                                        </button>
                                                    </div>
                                                    <!-- <div class="p-1 bd-highlight">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $id; ?>">
                                                            detail barang
                                                        </button>
                                                    </div> -->
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container fluid -->
</div> <!-- Page content Wrapper -->
<?php
$no = 1;
foreach ($data as $red) :
    $codeorder      = $red->ordercode;
    $nama_member    = $red->nama_member;
    $total_harga    = $red->all_total;
    $status         = $red->status_barang;
    $id             = $red->id;
    $alamat         = $red->alamat . ', ' . $red->district . ', ' . $red->province;
    $total          = $red->total_pesanan;

?>
    <div class="modal fade" id="modal<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="<?php echo base_url() . 'admin/laporanpenjualan/update/' . $id; ?>" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Data Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>No Pemesanan</label>
                                <input type="text" class="form-control" name="nama_produk" placeholder="order-<?php echo $codeorder; ?>" disabled />
                                <label>Nama Member</label>
                                <input type="text" class="form-control" name="member" placeholder="<?php echo $nama_member; ?>" disabled />
                                <label>Total Pesanan</label>
                                <input type="text" class="form-control" name="member" placeholder="<?php echo $total; ?>" disabled />
                                <label>Alamat</label>
                                <input type="text" class="form-control" name="member" placeholder="<?php echo $alamat; ?>" disabled />
                                <label>Total Harga</label>
                                <input type="text" class="form-control" name="member" placeholder="<?php echo $total_harga; ?>" disabled />
                                <label>Status</label>
                                <?php if ($status != 'settlement') : ?>
                                    <select name="optionstatus" class="form-control">
                                        <option value="0">Belum Bayar</option>
                                        <option value="1">Sudah Bayar</option>
                                        <!-- <option value="2">Sudah Sampai</option> -->
                                    </select>
                                <?php endif; ?>
                                <?php if ($status === 'settlement') : ?>
                                    <label>Status Barang</label>
                                    <select name="opsibarangstatus" class="form-control">
                                        <option value="Dikemas">Dikemas</option>
                                        <option value="Dikirim">Dikirim</option>
                                        <!-- <option value="Sudah Sampai">Sudah Sampai</option> -->
                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php endforeach; ?>