</div> <!-- content -->

<footer class="footer">
  © 2020 <b>AFQOZ Collection</b> <span class="d-none d-sm-inline-block"> <i class="mdi mdi-heart text-danger"></i>
    Just for you</span>
</footer>

</div>
<!-- End Right content here -->

</div>
<!-- END wrapper -->


<!-- Bottom Navbar -->
<nav class="navbar navbar-dark bg-primary navbar-expand fixed-bottom d-md-none d-lg-none d-xl-none">
  <ul class="navbar-nav nav-justified w-100">
    <li class="nav-item">
      <a href="#" class="nav-link">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
          <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
        </svg>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
          <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
        </svg>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-plus-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
          <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
        </svg>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
        </svg>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
        </svg>
      </a>
    </li>
  </ul>
</nav>


<!-- jQuery  -->
<script src="<?php echo base_url('assets/backend/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/detect.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/fastclick.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/jquery.slimscroll.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/jquery.blockUI.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/waves.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/jquery.nicescroll.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/jquery.scrollTo.min.js') ?>"></script>

<!-- skycons -->
<script src="<?php echo base_url('assets/backend/plugins/skycons/skycons.min.js') ?>"></script>

<!-- skycons -->
<script src="<?php echo base_url('assets/backend/plugins/peity/jquery.peity.min.js') ?>"></script>

<!--Morris Chart-->
<script src="<?php echo base_url('assets/backend/plugins/morris/morris.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/raphael/raphael-min.js') ?>"></script>

<!-- dashboard -->
<script src="<?php echo base_url('assets/backend/pages/dashboard.js') ?>"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url('assets/backend/plugins/sweet-alert2/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/pages/sweet-alert.init.js') ?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url('assets/backend/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>



<!-- Buttons examples -->
<script src="<?php echo base_url('assets/backend/plugins/datatables/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/buttons.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/pdfmake.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/vfs_fonts.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/buttons.print.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/buttons.colVis.min.js') ?>"></script>



<!-- Responsive examples -->
<script src="<?php echo base_url('assets/backend/plugins/datatables/dataTables.responsive.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datatables/responsive.bootstrap4.min.js') ?>"></script>

<!-- Datatable init js -->
<script src="<?php echo base_url('assets/backend/pages/datatables.init.js') ?>"></script>

<!-- Dropzone js -->
<script src="<?php echo base_url('assets/backend/plugins/dropzone/dist/dropzone.js') ?>"></script>

<!--Wysiwig js-->
<script src="<?php echo base_url('assets/backend/plugins/tinymce/tinymce.min.js') ?>"></script>

<!-- XEditable Plugin -->
<script src="<?php echo base_url('assets/backend/plugins/moment/moment.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/x-editable/js/bootstrap-editable.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/pages/xeditable.js') ?>"></script>

<!-- App js -->
<script src="<?php echo base_url('assets/backend/js/app.js') ?>"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script src="<?php echo base_url('assets/backend/js/script.js') ?>"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script>
  $(document).ready(function() {

    $('#modal-edit').on('show.bs.modal', function(event) {
      console.log('masuk')
      var button = $(event.relatedTarget)
      var id_produk = button.data('id_produk')
      var modal = $(this)
      $.ajax({
        url: "<?php echo base_url() ?>" + "admin/produk/getOneProduk/" + id_produk,
        method: "GET",
        success: function(result) {
          result = JSON.parse(result)
          modal.find('#id_produk').val(result.id_produk)
          modal.find('#nama_produk').val(result.nama_produk)
          modal.find(`#id_kategori option[value='${result.id_kategori.id_kategori}']`).attr('selected', "selected")
          modal.find('#harga').val(result.harga)
          // modal.find('#diskon').val(result.diskon)
          modal.find('#deskripsi').val(result.deskripsi)
          modal.find('#berat_produk').val(result.berat_produk)
          var foto_produk = JSON.parse(result.foto_produk)
          modal.find('#foto_produk').attr('src', "<?php echo base_url() . 'uploads/produk/' ?>" + foto_produk[0])
        },
        error: function(xhr) {

        }
      })
    })



    $('.btnupdateproduk').on('click', function(e) {
      console.log(e)
      $('#formupdateproduk').submit();
    })


    $('#modal-detail').on('show.bs.modal', function(event) {
      console.log('masuk')
      var button = $(event.relatedTarget)
      var id_produk = button.data('id_produk')
      var modal = $(this)
      $.ajax({
        url: "<?php echo base_url() ?>" + "admin/produk/getOneProduk/" + id_produk,
        method: "GET",
        success: function(result) {
          result = JSON.parse(result)
          var foto_produk = JSON.parse(result.foto_produk)
          $('#isidataproduk').html(`
            <label>Nama Produk : ${result.nama_produk}</label><br>
            <label>Kategori : ${result.id_kategori.nama_kategori}</label><br>
            <label>Deskripsi : ${result.deskripsi}</label><br>
            <label>Berat Produk : ${result.berat_produk} Kg</label><br>
            <label>Gambar Produk : </label>
            <div class="col-lg-5 col-md-6 col-xs-6 ">
              <img class="img-thumbnail w-100"
                src="<?php echo base_url() . 'uploads/produk/' ?>${foto_produk[0]}">
            </div>
            `)
        },
        error: function(xhr) {

        }
      })
      $.ajax({
        url: "<?php echo base_url() ?>" + "admin/stok/show/" + id_produk,
        method: "GET",
        success: function(result) {
          var tabledataukuran = $('#tabledataukuran')
          tabledataukuran.html('')
          result = JSON.parse(result)
          $.each(result, function(index, value) {
            tabledataukuran.append(`
              <tr>
                <td class="text-center">${value.ukuran_stok}</td>
                <td class="text-center">${value.jumlah_stok}</td>
                <td class="text-center">${value.harga}</td>
              </tr>
              `)
          })
        },
        error: function(xhr) {

        }
      })
      $('#formtambahstok').attr('action', `<?php echo base_url('admin/produk/addsize/') ?>` + id_produk)
    })

  })

  $('#downloadstok').click(function() {
    var pageTitle = 'Report Stok',
      stylesheet = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
      win = window.open('', 'Print', 'width=500,height=300');
    win.document.write('<html><head><title>' + pageTitle + '</title>' +
      '<link rel="stylesheet" href="' + stylesheet + '">' +
      '</head><body>' + $('.table')[0].outerHTML + '</body></html>');
    win.document.close();
    win.print();
    win.close();
    return false;
  });

  $('#fdate').datepicker({
    format: "dd-M-yy",
    todayHighlight:'TRUE',
    autoclose: true,
    minDate: 0,
    maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          $('#tdate').datepicker('setStartDate', $("#fdate").val());
  });

  
  $('#tdate').datepicker({
      format: "dd-M-yy",
      todayHighlight:'TRUE',
      autoclose: true,
      minDate: '0',
      maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          var start   = $("#fdate").val();
          var startD  = formatDate(new Date(start),'start')
          var end     = $("#tdate").val();
          var endD    = formatDate(new Date(end),'end')
          let payload = {
            start : startD,
            end   : endD
          }
          window.location = '<?php echo base_url('/admin/lapPemesanan'); ?>?dates='+startD+'&datend='+endD
          // var req = $.ajax({ 
          //     url: '<?php echo base_url('/admin/lapPemesanan'); ?>?dates='+startD+'&datend'+endD,
          //     type: 'POST',
          //     data: payload,
          //     dataType: 'json',
          //     success: function(response) { 
          //       console.log(response.data);
          //       $('#datatable').DataTable(response.data).fnDestroy();
          //       $('#datatable').DataTable(response.data);
          //     }
          // });

        //   $('#example').DataTable( {
        //     "ajax": '../ajax/data/arrays.txt'
        // } );

  });

  $('#sdate').datepicker({
    format: "dd-M-yy",
    todayHighlight:'TRUE',
    autoclose: true,
    minDate: 0,
    maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          $('#sedate').datepicker('setStartDate', $("#sdate").val());
  });

  
  $('#sedate').datepicker({
      format: "dd-M-yy",
      todayHighlight:'TRUE',
      autoclose: true,
      minDate: '0',
      maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          var start   = $("#sdate").val();
          var startD  = formatDate(new Date(start),'start')
          var end     = $("#sedate").val();
          var endD    = formatDate(new Date(end),'end')
          let payload = {
            start : startD,
            end   : endD
          }
         
          window.location = '<?php echo base_url('/admin/Stok'); ?>?dates='+startD+'&datend='+endD

  });

  $('#mdate').datepicker({
    format: "dd-M-yy",
    todayHighlight:'TRUE',
    autoclose: true,
    minDate: 0,
    maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          $('#medate').datepicker('setStartDate', $("#mdate").val());
  });

  
  $('#medate').datepicker({
      format: "dd-M-yy",
      todayHighlight:'TRUE',
      autoclose: true,
      minDate: '0',
      maxDate: '+1Y+6M'
  }).on('changeDate', function (ev) {
          var start   = $("#mdate").val();
          var startD  = formatDate(new Date(start),'start')
          var end     = $("#medate").val();
          var endD    = formatDate(new Date(end),'end')
          let payload = {
            start : startD,
            end   : endD
          }
          window.location = '<?php echo base_url('/admin/LapMember'); ?>?dates='+startD+'&datend='+endD

  });


  function formatDate(date,condition) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    if(condition == 'start') {
      return [year, month, day].join('-') + " 00:00:01";
    } else {
      return [year, month, day].join('-') + " 23:59:59";
    }
   
}

function dropdown() {
  console.log('masuk')
  var x = document.getElementById("ukuran-dp").value;
}

    // window.onload = function() {

    //   //Better to construct options first and then pass it as a parameter
    //   var options = {
    //     title: {
    //       text: "Column Chart in jQuery CanvasJS"
    //     },
    //     data: [{
    //       // Change type to "doughnut", "line", "splineArea", etc.
    //       type: "column",
    //       dataPoints: [{
    //           label: "apple",
    //           y: 10
    //         },
    //         {
    //           label: "orange",
    //           y: 15
    //         },
    //         {
    //           label: "banana",
    //           y: 25
    //         },
    //         {
    //           label: "mango",
    //           y: 30
    //         },
    //         {
    //           label: "grape",
    //           y: 28
    //         }
    //       ]
    //     }]
    //   };

    //   $("#chartContainer").CanvasJSChart(options);
    // }

</script>

</body>

</html>