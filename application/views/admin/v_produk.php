<div class="page-content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="float-right page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">KuzCommerce</a></li>
						<li class="breadcrumb-item"><a href="#">Data Produk</a></li>
						<li class="breadcrumb-item active">Detil Data Produk</li>
					</ol>
				</div>
				<h5 class="page-title">Detil dan Data Produk</h5>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-sm-6">
				<div class="card m-b-30">
					<div class="card-body">

						<?php echo $this->session->flashdata('message'); ?>
						<div class="table-responsive">
							<div class="text-center p-4 float-right">

								<button type="button" class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-tambah"><i class="mdi mdi-plus p-1 font-weight-bold"></i><span>Tambah
										Data</span></button>
							</div>


							<div class="table-responsive">
								<table id="datatable" class="table table-bordered dt-responsive  text-justify" style="width:100%">
									<thead>
										<tr>
											<th class="text-center">No</th>
											<th class="text-center">Nama Produk</th>
											<th class="text-center">Kategori</th>
											<th class="text-center">Thumbnail</th>
											<th class="text-center">Deskripsi</th>
											<th class="text-center">Berat Produk</th>
											<th class="text-center">Aksi</th>
										</tr>
									</thead>
									<tbody class="table-striped">
										<?php
										$no = 1;
										foreach ($data as $i) :
											$id_produk = $i->id_produk;
											$nama_produk = $i->nama_produk;
											$nama_kategori = $i->nama_kategori;
											$foto_produk = json_decode($i->foto_produk);
											// $harga = $i['harga'];
											// $diskon = $i['diskon'];
											$deskripsi = $i->deskripsi;
											$berat_produk = $i->berat_produk;
										?>
											<tr>
												<th class="text-center" scope="row"><?php echo $no++; ?></th>
												<td class="text-center"><?php echo $nama_produk; ?></td>
												<td class="text-center"><?php echo $nama_kategori; ?></td>
												<td class="text-center"><img class="img-thumbnail w-50" src="<?php echo base_url() . 'uploads/produk/' . $foto_produk[0]; ?>"></td>
												<td class="text-center"><?php echo $deskripsi; ?></td>
												<td class="text-center"><?php echo $berat_produk; ?></td>


												<td class="text-center">
													<span data-toggle="modal" data-target="#modal-detail" data-id_produk="<?php echo $id_produk ?>">
														<button type="button" class="btn btn-small btn-info" data-placement="top" title="Detail Data" data-toggle="tooltip">
															<i class="mdi mdi-clipboard-text font-weight-bold text-white"></i>
														</button>
													</span>

													<span data-toggle="modal" data-target="#modal-edit" data-id_produk="<?php echo $id_produk ?>">
														<button type="button" class="btn btn-small btn-warning" data-placement="top" title="Edit Data" data-toggle="tooltip">
															<i class="mdi mdi-lead-pencil font-weight-bold text-white"></i>
														</button>
													</span>

													<span data-toggle="modal" data-target="#modal-hapus<?php echo $id_produk; ?>">
														<button type="button" class="btn btn-small btn-danger" data-placement="top" title="Hapus Data" data-toggle="tooltip">
															<i class="mdi mdi-delete font-weight-bold text-white"></i>
														</button>
													</span>

												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- batas nampilin tabel -->

<!-- tambah data nih -->
<div id="modal-tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myModalLabel">Tambah Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?php echo base_url() . 'admin/produk/save' ?>" method="post" enctype="multipart/form-data">
				<div class="modal-body">

					<div class="form-group">
						<label>Nama Produk</label>
						<input type="text" class="form-control" name="nama_produk" placeholder="Masukkan nama Produk" required />
					</div>
					<div class="form-group">
						<label>Kategori</label>
						<div class="form-group">
							<select class="form-control" name="id_kategori" required>
								<?php foreach ($combo as $cmb) { ?>
									<option value="<?php echo $cmb->id_kategori ?>">
										<?php echo $cmb->nama_kategori ?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group mb-2">
						<label>Foto Produk (Multiple Input)</label>
						<div class="custom-file">
							<label class="custom-file-label" for="customFile">Pilih Multiple Foto</label>
							<input type="file" multiple="" class="custom-file-input" id="customFile" name="foto_produk[]" required>
						</div>
					</div>
					<!-- <div class="form-group">
						<label>Diskon</label>
						<input type="text" class="form-control" name="diskon" placeholder="Masukkan diskon" required />
					</div> -->
					<div class="form-group">
						<label>Deskripsi</label>
						<input type="text" class="form-control" name="deskripsi" placeholder="Masukkan deskripsi" required />
					</div>
					<div class="form-group">
						<label>Nilai Bobot</label>
						<div>
							<input type="number" name="berat_produk" class="form-control" step="0.1" min="0.1" placeholder="Masukkan berat produk (Kg)" required />
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="float-right">
						<button type="button" data-dismiss="modal" class="btn btn-secondary waves-effect m-l-5">
							Batal
						</button>

						<button type="submit" class="btn btn-primary waves-effect waves-light">
							Tambah
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- batas modal buat input data -->




<!-- DETAIL MODAL -->
<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myModalLabel">Detail Stok Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>

			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
					<div class="card m-b-30">
						<div class="card-body">
							<div class="form-group" id="isidataproduk">

							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-7">
					<div class="card m-b-30">
						<div class="card-body">
							<b>Tambah Stok</b>
							<hr>
							<form action="" method="post" enctype="multipart/form-data" id="formtambahstok">
								<div class="input-group input-group-sm mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="inputGroup-sizing-sm">Ukuran</span>
										<div style="margin-left: 12px;" >
											<select style="padding-left: 12px;background:#f2f5f7" name="ukuran" id="ukuran-dp">
												<option value="S">S</option>
												<option value="M">M</option>
												<option value="L"">L</option>
												<option value="XL">XL</option>
											</select>
											<span class="focus"></span>
										</div>
									</div>
								</div>
								<div class="input-group input-group-sm mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="inputGroup-sizing-sm"> Jumlah</span>
									</div>
									<input type="number" name="jml" class="form-control" min="1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
								</div>

								<div class="input-group input-group-sm mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="inputGroup-sizing-sm">Harga Satuan</span>
									</div>
									<input type="number" name="harga" class="form-control" min="1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
								</div>

								<div class="form-group mt-4">
									<div>
										<button type="submit" class="btn btn-primary btn-lg waves-effect waves-light btn-block">
											SAVE
										</button>
									</div>
								</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<div class=" container table-responsive">
				<header>
					<h6>Data Stok pada Produk :</h6>
				</header>
				<table id="datatable" class="table table-bordered dt-responsive  text-justify" style="width:100%">
					<thead>
						<tr>
							<th class="text-center">Ukuran</th>
							<th class="text-center">Qty</th>
							<th class="text-center">Harga Satuan</th>
						</tr>
					</thead>
					<tbody id="tabledataukuran">

					</tbody>
				</table>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
		</div>

	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="modal-edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="<?php echo base_url() . 'admin/produk/apdet' ?>" method="post" enctype="multipart/form-data" id="formupdateproduk">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Edit Data</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="id_produk" name="id_produk">
					<div class="form-group">
						<label>Nama Produk</label>
						<input type="text" class="form-control" name="nama_produk" value="" id="nama_produk" required />
					</div>

					<div class="form-group">
						<label>Kategori</label>
						<div class="form-group">
							<select class="form-control" name="id_kategori" required id="id_kategori">
								<?php foreach ($combo as $cmb) { ?>

									<option value="<?php echo $cmb->id_kategori ?>">
										<?php echo $cmb->nama_kategori ?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>

					<!-- <div class="form-group">
						<label>Diskon</label>
						<input type="text" class="form-control" name="diskon" placeholder="" required id="diskon" />
					</div> -->

					<div class="form-group">
						<label>Deskripsi</label>
						<input type="text" class="form-control" name="deskripsi" placeholder="" required id="deskripsi" />
					</div>
					<div class="form-group">
						<label>Nilai Bobot</label>
						<div>
							<input type="number" name="berat_produk" class="form-control" step="0.1" placeholder="" required id="berat_produk" />
						</div>
					</div>
					<div class="form-group mb-2">
						<label>Thumbnail Artikel</label>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-xs-6 w-25 h-25">
								<img class="img-thumbnail w-75 h-75" src="" id="foto_produk">
								<input type="hidden" value="" name="gambar">
							</div>
							<div class="col-lg-6 col-md-6 col-xs-6">
								<div class="custom-file">
									<label class="custom-file-label" for="foto_produk">Pilih file Foto</label>
									<input type="file" class="custom-file-input" id="custom-file" name="foto_produk[]" multiple="multiple">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="float-right">
						<button type="button" data-dismiss="modal" class="btn btn-secondary waves-effect m-l-5">
							Batal
						</button>
						<button type="submit" class="btn btn-primary waves-effect waves-light btnupdateproduk">
							Update
						</button>
					</div>
				</div>
			</form>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<!-- batas modal edit -->

<?php
foreach ($data as $i) :
	$id_produk = $i->id_produk;
	$nama_produk = $i->nama_produk;
	$nama_kategori = $i->nama_kategori;
	$foto_produk = json_decode($i->foto_produk);
	// $harga = $i['harga'];
	// $diskon = $i['diskon'];
	$deskripsi = $i->deskripsi;
	$berat_produk = $i->berat_produk;
?>

	<!-- Modal Hapus -->
	<div id="modal-hapus<?php echo $id_produk; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h5 class="modal-title mt-0 " id="myModalLabel">Hapus Data Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="<?php echo base_url() . 'admin/produk/delete/' . $id_produk; ?>" method="post">
					<div class="modal-body">
						<p>Anda yakin mau menghapus <b class="h6 bg-danger text-white pr-2 pl-2"><?php echo $nama_produk; ?></b> dari data
							Produk?</p>
						<p class="text-danger text-left pt-1 mb-0">*Data yang dihapus tidak akan bisa
							dikembalikan</p>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
						<button class="btn btn-danger">Hapus</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>