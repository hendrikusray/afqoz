<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">AFQOZ</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h5 class="page-title">Selamat datang, Admin!
                </h5>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat m-b-30">
                    <div class="p-3 bg-primary text-white">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-clipboard-text float-right mb-0"></i>
                        </div>
                        <h6 class="text-uppercase mb-0">Total Artikel</h6>
                    </div>
                    <div class="card-body">
                        <div class="mt-2 text-muted">
                            <div class="float-right">
                                <span class="ml-2 text-muted">Postingan Artikel</span>
                                <?php $result = $this->db->get('artikel');
                                $jumlah = $result->num_rows();
                                ?>

                            </div>
                            <h4 class="m-0 text-center"><?php echo $jumlah; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat m-b-30">
                    <div class="p-3 bg-primary text-white">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cube-outline float-right mb-0"></i>
                        </div>
                        <h6 class="text-uppercase mb-0">Total Produk</h6>
                    </div>
                    <div class="card-body">
                        <div class="mt-2 text-muted">
                            <div class="float-right">
                                <span class="ml-2 text-muted">Jumlah Produk</span>
                                <?php $result = $this->db->get('produk');
                                $jumlah = $result->num_rows();
                                ?>

                            </div>
                            <h4 class="m-0 text-center"><?php echo $jumlah; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat m-b-30">
                    <div class="p-3 bg-primary text-white">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-account-network float-right mb-0"></i>
                        </div>
                        <h6 class="text-uppercase mb-0">Total Member</h6>
                    </div>
                    <div class="card-body">
                        <div class="mt-2 text-muted">
                            <div class="float-right">
                                <span class="mr-3 text-muted"> Member Aplikasi</span>
                                <?php $result = $this->db->get('member');
                                $jumlah = $result->num_rows();
                                ?>

                            </div>
                            <h4 class="m-0 text-center"><?php echo $jumlah; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card mini-stat m-b-30">
                    <div class="p-3 bg-primary text-white">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-cart-outline float-right mb-0"></i>
                        </div>
                        <h6 class="text-uppercase mb-0">Total Transaksi</h6>
                    </div>
                    <div class="card-body">
                        <div class="mt-2 text-muted">
                            <div class="float-right">
                                <span class="ml-2 text-muted"> Jumlah Transaksi </span>
                                <?php $result = $this->db->get('pembayaran');
                                $jumlah = $result->num_rows();
                                ?>
                            </div>
                            <h4 class="m-0"><?php echo $jumlah; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div id="chartContainerS" style="height: 370px; width: 100%;"></div>
            </div>
            <div class="col-6">
                <div id="chartContainerTransaksi" style="height: 370px; width: 100%;"></div>
            </div>
        </div>
        <script>
            window.onload = function() {

                //Better to construct options first and then pass it as a parameter
                var options = {
                    title: {
                        text: "Penjualan Dalam Sebulan Transaksi"
                    },
                    data: [{
                        // Change type to "doughnut", "line", "splineArea", etc.
                        type: "column",
                        dataPoints: [{
                                label: "Januari",
                                y:  <?php echo $bulan['januari'] === null ? 0 : $bulan['januari'] ?>
                            },
                            {
                                label: "Febuari",
                                y:  <?php echo $bulan['febuari'] === null ? 0 : $bulan['expired'] ?>
                            },
                            {
                                label: "Maret",
                                y:  <?php echo $bulan['maret'] === null ? 0 : $bulan['maret'] ?>
                            },
                            {
                                label: "April",
                                y:  <?php echo $bulan['april'] === null ? 0 : $bulan['april'] ?>
                            },
                            {
                                label: "Mei",
                                y:  <?php echo $bulan['mei'] === null ? 0 : $bulan['mei'] ?>
                            },
                            {
                                label: "Juni",
                                y:  <?php echo $bulan['juni'] === null ? 0 : $bulan['juni'] ?>
                            },
                            {
                                label: "Juli",
                                y:  <?php echo $bulan['juli'] === null ? 0 : $bulan['juli'] ?>
                            },
                            {
                                label: "Agustus",
                                y:  <?php echo $bulan['agustus'] === null ? 0 : $bulan['agustus'] ?>
                            },
                            {
                                label: "September",
                                y:  <?php echo $bulan['september'] === null ? 0 : $bulan['september'] ?>
                            },
                            {
                                label: "Oktober",
                                y:  <?php echo $bulan['oktober'] === null ? 0 : $bulan['oktober'] ?>
                            },
                            {
                                label: "November",
                                y:  <?php echo $bulan['november'] === null ? 0 : $bulan['november'] ?>
                            },
                            {
                                label: "Desember",
                                y:  <?php echo $bulan['desember'] === null ? 0 : $bulan['desember'] ?>
                            }
                        ]
                    }]
                };
                 //Better to construct options first and then pass it as a parameter
                 var optionsTransaksi = {
                    title: {
                        text: "Penjualan Transaksi"
                    },
                    data: [{
                        // Change type to "doughnut", "line", "splineArea", etc.
                        type: "column",
                        dataPoints: [{
                                label: "Expired",
                                y:  <?php echo $data['expired'] === null ? 0 : $data['expired'] ?>
                            },
                            {
                                label: "Belum Dibayar",
                                y:  <?php echo $data['belum_bayar'] === null ? 0 : $data['belum_bayar'] ?>
                            },
                            {
                                label: "Sudah Dibayar",
                                y:  <?php echo $data['sudah_bayar'] === null ? 0 : $data['sudah_bayar'] ?>
                            }
                        ]
                    }]
                };

                $("#chartContainerTransaksi").CanvasJSChart(optionsTransaksi);

                $("#chartContainerS").CanvasJSChart(options);
            }
        </script>
        <!-- end row -->