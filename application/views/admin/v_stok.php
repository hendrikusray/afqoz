<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Afqoz</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active">Stok</li>
                    </ol>
                </div>
                <h5 class="page-title">Laporan Stok</h5>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Semua data stok</h4>
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <span>Dari Tanggal</span>
                                    <input class="form-control datepicker" type="text" id="sdate" name="sdate">
                                </div>
                                <div class="col">
                                    <span>Sampai Tanggal</span>
                                    <input class="form-control datepicker" type="text" id="sedate" name="sedate">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end"><button type="submit" id="downloadstok" class="btn btn-primary">download</button></div>
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered dt-responsive  text-justify" style="width:100%">
                                <thead>
                                    <tr>
                                        <th  style=" border: 1px solid black;" class="text-center">No</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Nama Produk</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Nama Kategori</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Ukuran</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Qty </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($data->result_array() as $i) :

                                        $id_produk = $i['nama_produk'];
                                        $ukuran_stok = $i['ukuran_stok'];
                                        $jumlah_stok = $i['jumlah_stok'];
                                        $nama_kategori = $i['nama_kategori'];
                                    ?>
                                        <tr>
                                            <th scope="row" class="text-center"  style=" border: 1px solid black;"><?php echo $no++; ?></th>

                                            <td  style=" border: 1px solid black;"><?php echo $id_produk; ?></td>
                                            <td  style=" border: 1px solid black;" class="text-center"><?php echo empty($nama_kategori) ? '-' :  $nama_kategori; ?></td>
                                            <td  style=" border: 1px solid black;" class="text-center"><?php echo empty($ukuran_stok) ? '-' :  $ukuran_stok; ?></td>
                                            <td  style=" border: 1px solid black;" class="text-center"><?php echo empty($jumlah_stok) ? '0' :  $jumlah_stok; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container fluid -->
</div> <!-- Page content Wrapper -->