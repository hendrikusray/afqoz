<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="float-right page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">KPTI</a></li>
						<li class="breadcrumb-item"><a href="#">Rangking</a></li>
						<li class="breadcrumb-item active">Data & Hasil Perangkingan</li>
					</ol>
				</div>
				<h5 class="page-title">Data & Hasil Perangkingan</h5>
			</div>
		</div>
		<!-- end row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-6">
				<div class="card m-b-30">
					<div class="card-body">
						<div class="tab-pane p-3" id="profile" role="tabpanel">
							<h4 class="mt-0 header-title">Laporan Laba</h4>
							<p class="text-muted m-b-30 font-14">The Buttons extension for
								DataTables
								provides a common set of options, API methods and styling to
								display
								buttons on a page that will interact with a DataTable. The
								core library
								provides the based framework upon which plug-ins can built.
							</p>

							<table id="datatable-buttons"
								class="table table-striped table-bordered dt-responsive nowrap"
								style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
									<tr>
										<th>Name</th>
										<th>Position</th>
										<th>Office</th>
										<th>Age</th>
										<th>Start date</th>
										<th>Salary</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>Tiger Nixon</td>
										<td>System Architect</td>
										<td>Edinburgh</td>
										<td>61</td>
										<td>2011/04/25</td>
										<td>$320,800</td>
									</tr>
									<tr>
										<td>Donna Snider</td>
										<td>Customer Support</td>
										<td>New York</td>
										<td>27</td>
										<td>2011/01/25</td>
										<td>$112,000</td>
									</tr>
								</tbody>
							</table>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- end row -->
</div><!-- container fluid -->
</div> <!-- Page content Wrapper -->
