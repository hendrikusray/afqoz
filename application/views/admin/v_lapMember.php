<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Afqoz</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active">Member</li>
                    </ol>
                </div>
                <h5 class="page-title">Laporan Member</h5>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Semua Data Member</h4>
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <span>Dari Tanggal</span>
                                    <input class="form-control datepicker" type="text" id="mdate" name="mdate">
                                </div>
                                <div class="col">
                                    <span>Sampai Tanggal</span>
                                    <input class="form-control datepicker" type="text" id="medate" name="medate">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end"><button type="submit" id="downloadstok" class="btn btn-primary">download</button></div>
                        <!-- <div class="d-flex justify-content-end"><button type="submit" class="btn btn-primary">download</button></div> -->
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered dt-responsive  text-justify" style="width:100%">
                                <thead>
                                    <tr>
                                        <th  style=" border: 1px solid black;" class="text-center">No</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Nama Member</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Email</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Alamat</th>
                                        <th  style=" border: 1px solid black;" class="text-center">Status</th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php
                                    $no = 1;
                                    foreach ($data as $red) :
                                        $nama_member      = $red->nama_member;
                                        $email            = $red->email;
                                        $alamat           = $red->alamat;
                                        $status           = $red->status;
                                        if($status == 1) {
                                            $status = 'aktif';
                                        } else {
                                            $status = 'tidak aktif';
                                        }
                                    ?>

                                        <tr>
                                            <th scope="row" class="text-center"  style=" border: 1px solid black;"><?php echo $no++; ?></th>
                                            <td  style=" border: 1px solid black;"><?php echo $nama_member; ?></td>
                                            <td  style=" border: 1px solid black;"><?php echo $email; ?></td>
                                            <td  style=" border: 1px solid black;"><?php echo $alamat; ?></td>
                                            <td  style=" border: 1px solid black;"><?php echo $status; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container fluid -->
</div> <!-- Page content Wrapper -->