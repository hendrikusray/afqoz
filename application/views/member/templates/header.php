	<?php
	$hal = $this->uri->segment(1);
	?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">

		<title>AFQOZ Collection+</title>
		<meta content="" name="descriptison">
		<meta content="" name="keywords">

		<!-- Favicons -->
		<link href="<?php echo base_url('assets/backend/images/afqoz.png') ?>" rel="icon">
		<link href="<?php echo base_url('assets/backend/images/afqoz.png') ?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

		<!-- Vendor CSS Files -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-server-Tr-wluH4NBzKQTBZ0IgyogSd"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
		<link href="<?php echo base_url('assets/frondend/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/animate.css/animate.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/venobox/venobox.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/owl.carousel/assets/owl.carousel.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/slick/slick.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/vendor/slick/slick-theme.css') ?>" rel="stylesheet">

		<!-- Template Main CSS File -->
		<link href="<?php echo base_url('assets/frondend/css/style.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/frondend/css/ribs.css') ?>" rel="stylesheet">



		<!-- style tambahan buat nav mobile -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/backend/images/afqoz.png') ?>">
		<link href="<?php echo base_url('assets/backend/css/icons.css') ?>" rel="stylesheet" type="text/css">
		<!-- <script src="<?php echo base_url('assets/backend/js/app.js') ?>"></script> -->
		<!-- <link href="<?php echo base_url('assets/frondend/css/cart.css') ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/frondend/js/cart.js') ?>"></script>
	<link href="<?php echo base_url('assets/frondend/css/prettyPhoto.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/frondend/css/modal-custom.css') ?>" rel="stylesheet"> -->


		<style>
			#lblCartCount {
				font-size: 12px;
				background: #ff0000;
				color: #fff;
				padding: 0 5px;
				vertical-align: top;
			}

			.modal {
				position: fixed;
				top: 1;
				left: 0;
				z-index: 1050;
				display: none;
				width: 100%;
				height: 100%;
				overflow: hidden;
				outline: 0;
			}

			.modal.right .modal-dialog {
				position: fixed;
				margin: auto;
				width: 85%;
				height: 100%;
				-webkit-transform: translate3d(0%, 0, 0);
				-ms-transform: translate3d(0%, 0, 0);
				-o-transform: translate3d(0%, 0, 0);
				transform: translate3d(0%, 0, 0);
			}

			.modal.right .modal-content {
				height: 100%;
				overflow-y: auto;
			}

			.modal.right .modal-body {
				padding: 15px 15px 80px;
			}

			/*Right*/
			.modal.right.fade .modal-dialog {
				/* right: -320px; */
				right: 0px;
				-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
				-moz-transition: opacity 0.3s linear, right 0.3s ease-out;
				-o-transition: opacity 0.3s linear, right 0.3s ease-out;
				transition: opacity 0.3s linear, right 0.3s ease-out;
			}

			.modal.right.fade.in .modal-dialog {
				right: -320px;
				/* right: 0; */
			}

			/* ----- MODAL STYLE ----- */
			.modal-content {
				border-radius: 10;
				border: none;
			}

			.modal-header {
				border-bottom-color: #EEEEEE;
				background-color: #FAFAFA;
			}
		</style>


	</head>
	<!-- ======= Header ======= -->
	<header id="header">

		<div class="container">
			<div class="logo float-left">
				<!-- Uncomment below if you prefer to use an image logo -->
				<h1 class="text-light"><a href="<?php echo base_url() ?>" class="scrollto"><span>AFQOZ </span></a></h1>
			</div>


			<nav class="main-nav float-right d-none d-lg-block">
				<ul>
					<li class="<?= ($hal == 'member/beranda') ? 'active' : ''; ?>"><a href="<?php echo base_url('member/beranda') ?>">Beranda</a></li>
					<li class="<?= ($hal == 'member/produk') ? 'active' : ''; ?>"><a href="<?php echo base_url('member/produk') ?>">Produk</a></li>
					<li class="<?= ($hal == 'member/artikel') ? 'active' : ''; ?>"><a href="<?php echo base_url('member/artikel') ?>">Artikel</a></li>

					<li class="nav-item ">
						<a href="<?= site_url('member/keranjang/addTocart') ?>" >
							<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-cart3" fill="black" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z">
								</path>
							</svg>
						</a>
					</li>
					<?php
					if ($this->session->userdata('user')) {
					?>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-user-circle"></i> My Account

							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item text-dark" href="<?php echo base_url('member/akunku') ?>"><i class="mdi mdi-camera-front-variant"></i> <?= @$_SESSION['user']; ?></a>
								
								<a class="dropdown-item text-dark" href="<?php echo base_url('member/keranjang/myorder') ?>"><i class="mdi mdi-bulletin-board"></i> Riwayat Pesan</a>
								<a class="text-dark" href="<?= site_url('login/logout') ?>"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
							</div>
						</li>
					<?php
					} else {
					?>
						<li class="nav-item">
							<a href="<?php echo base_url('login') ?>" class="btn btn-sm btn-primary mr-2 ml-2 mb-1 mt-1" style="color: white;">
								<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>LogIn</a>
						</li>
						</li>
					<?php
					}
					?>
					</li>
				</ul>
			</nav><!-- .main-nav -->
		</div>
	</header><!-- #header -->

	<!-- modal info -->
	<div id="tentang-kami" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> <br><br>
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Afqoz Collection's</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h4>Afqoz</h4>
					<p>Afqos Collection berdiri sejak 2015, kami menyediakan berbagai macam macam baju baju anak,
						remaja, dewasa, dan lansia sesuai dengan kebutuhan anda.
						Harga yang kami tawarkan sangat terjangkau.</p>

					<h4>Business Hours </h4>
					<p>
						Setiap hari : 8:00 AM – 9:00 PM
					</p>

					<h4>Info Lokasi</h4>
					<iframe class="container" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15841.092408237304!2d109.1211586930205!3d-6.977069316357292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6fbfa1185f5d33%3A0xaa1386e6650072fd!2sAFQOZ%20collection!5e0!3m2!1sid!2sid!4v1605867468564!5m2!1sid!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->



	<!-- Modal keranjang -->
	<section>
		<div class="container modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
			<div class="modal-dialog" role="document">
				<div id="keranjang" class="modal-content">

					<div class="modal-header p-3">
						<div class="touch-indicator"></div>
						<div class="modal-title unicon" id="myModalLabel2"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" fill="#60051f">
								<path d="M8.5,19A1.5,1.5,0,1,0,10,20.5,1.5,1.5,0,0,0,8.5,19ZM19,16H7a1,1,0,0,1,0-2h8.49121A3.0132,3.0132,0,0,0,18.376,11.82422L19.96143,6.2749A1.00009,1.00009,0,0,0,19,5H6.73907A3.00666,3.00666,0,0,0,3.92139,3H3A1,1,0,0,0,3,5h.92139a1.00459,1.00459,0,0,1,.96142.7251l.15552.54474.00024.00506L6.6792,12.01709A3.00006,3.00006,0,0,0,7,18H19a1,1,0,0,0,0-2ZM17.67432,7l-1.2212,4.27441A1.00458,1.00458,0,0,1,15.49121,12H8.75439l-.25494-.89221L7.32642,7ZM16.5,19A1.5,1.5,0,1,0,18,20.5,1.5,1.5,0,0,0,16.5,19Z">
								</path>
							</svg></div>
						<h5> Keranjang</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
								<h6> &times; Close</h6>
							</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="v-card__text">
							<div class="pt-2">
								<div>
									<div class="">
										<div class="layout align-center">
											<div class="flex--wrap">
												<div>
													<div class="v-image v-responsive theme--light" img-width="128" style="width: 48px;">
														<div class="v-responsive__sizer" style="padding-bottom: 100%;">
														</div>
														<div>
															<img src="" class="d-block w-100 boradius" alt="" srcset="">
														</div>
														<div class="v-responsive__content"></div>
													</div>
												</div>
											</div>
											<div class="flex flex--fill o-hidden ml-2">
												<div class="font-weight-bold lines-1">
													<!-- <?php echo $details[0]->deskripsi ?> -->
												</div>
												<div class="caption light--text">
													<div class="lines-1">
														<!-- <?php echo $details[0]->nama_produk ?> -->
													</div>
													<div class="lines-1">
														<!-- Rp. <?php echo $details[0]->harga ?> -->
													</div>
												</div>
											</div>
											<div class="font-weight-bold mx-1 flex--wrap">
												1
											</div> <button type="button" class="v-btn--light flex--wrap noshadow ml-2 v-btn v-btn--contained theme--light v-size--small"><span class="v-btn__content">
													X
												</span></button>
										</div>
									</div>
									<hr role="separator" aria-orientation="horizontal" class="v-divider theme--light">
								</div>
								<button id="btn-checkout" type="button" class="my-4 noshadow v-btn v-btn--block v-btn--contained theme--light v-size--default">
									<a href="<?php echo base_url('member/checkout') ?>">
										<span class="v-btn__content">
											🌼✨ Lanjutkan Checkout 🚀 🔪🔪 🎃
										</span>
									</a>
								</button>
							</div>
						</div>
					</div><!-- modal-dialog -->
				</div><!-- modal -->
			</div>
	</section>