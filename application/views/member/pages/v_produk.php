	<!-- ======= Produk Section ======= -->
	<section id="produk" class="produk ">
		<div class="container">
			<header class="section-header">
				<h6><b>Kategori Produk </b></h6>
				<div class="dropdown">
					<button style="color:white" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php 
							if($check == 'false') {
								echo "All Category";
							} else {
								echo $check;
							}
						?>
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="<?php echo base_url() . 'member/produk' ?>">All Category</a>
						<?php
						foreach ($kategori as $i) :
							$nama_kategori = $i->nama_kategori;
							$id_kategori   = $i->id_kategori;
						?>

							<a class="dropdown-item" href="<?php echo base_url() . 'member/produk?q=' . $id_kategori ?>"><?php echo $nama_kategori; ?></a>
						<?php endforeach; ?>
					</div>
				</div>
			</header>
			
			<div class="row section-bg">
				<?php
				foreach ($data as $i) :
					$id_produk = $i->id_produk;
					$nama_produk = $i->nama_produk;
					$id_kategori = $i->id_kategori;
					$foto_produk = json_decode($i->foto_produk);
					// $harga = $i->harga;
					// $diskon = $i->diskon;
					$deskripsi = $i->deskripsi;
					$berat_produk = $i->berat_produk;
				?>
					<div class="col-6 col-md-4 col-lg-3  wow bounceInUp " data-wow-duration="1.4s">
						<div class="box">
							<div class="card ">
								<a href="<?= base_url('member/produk/detail_produk/' . $id_produk) ?>">
									<img class="card-img-top fotoproduk" src="<?php echo base_url() . 'uploads/produk/' . $foto_produk[0]; ?>" alt="Foto Produk">
									<div class="card-body p-3">
										<h5 class="card-title mb-1 font-weight-bold" style="font-size: 14px;"><b>
												<?php echo word_limiter($nama_produk, 10); ?></b></h5>
									
										<button type="button" class="btn btn-sm btn-outline-info">Pilih Produk</button>
									</div>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<!-- End Produk Section -->