<!--Section: Block Content-->
<div class="container">
    <section>
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-8">
                <!-- Card -->
                <div class="mb-3">
                    <div class="pt-4 wish-list">
                        <h5 class="mb-4">Cart (<span><?php echo $count->total_cart ?></span> items)</h5>
                        <?php $i = 0 ?>
                        <?php foreach ($data as $row) { ?>
                            <?php $i++ ?>
                            <div class="row mb-4" style="background-color: #ccc;" data-index="<?= $i ?>">
                                <input type="checkbox" checked data-input="<?= $row['id_produk'] ?>|<?= $row['qty'] ?>|<?= $row['id'] ?>" class="form-check-input" style="transform: scale(1.2)" id="checkboxselected">
                                <div class="col-md-5 col-lg-3 col-xl-3">
                                    <div class="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <?php foreach (json_decode($row['foto_produk']) as $key => $v) : ?>
                                                    <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                                        <img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" class="d-block w-100 boradius" alt="...">
                                                    </div>
                                                <?php endforeach ?>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-9 col-xl-9">
                                    <div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5><?php echo $row['nama_produk']; ?></h5>
                                                <p class="mb-3 text-muted text-uppercase small">Size: <?php echo $row['ukuran']; ?></p>
                                            </div>
                                            <div>
                                                <div class="def-number-input number-input safari_only mb-0 w-100">
                                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus decrease"></button>
                                                    <input class="quantity" min="0" name="quantity" value="<?php echo $row['qty']; ?>" type="number">
                                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus increase"></button>
                                                </div>
                                                <small id="passwordHelpBlock" class="form-text text-muted text-center">
                                                    (Note, 1 piece)
                                                </small>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <a data-remove="<?= $row['id_produk'] ?>|<?= $row['qty'] ?>|<?= $row['id'] ?>" type="button" id="removeitem" class="card-link-secondary small text-uppercase mr-3"><i class="fas fa-trash-alt mr-1"></i> Remove item </a>
                                            </div>
                                            <p class="mb-0"><span><strong id="summary">Rp. <?php echo $row['harga_stok']; ?></strong></span></p class="mb-0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- Card -->
                <!-- Card -->
                <div class="mb-3">
                    
                </div>
                <!-- Card -->
            </div>
            <!--Grid column-->
            <!--Grid column-->
            <div class="col-lg-4">
                <!-- Card -->
                <div class="mb-3">
                    <div class="pt-4">

                        <h5 class="mb-3">Total Pembayaran</h5>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                Total
                                <span> <?php echo $count->total_produk ?> produk</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                <div>
                                    <strong>Total Pembayaran</strong>
                                    
                                </div>
                                <span><strong>Rp.<?php echo $count->total ?></strong></span>
                            </li>
                        </ul>
                        <button type="submit" id="gotocheckout"  <?php if (empty($data)){ ?> disabled <?php   } ?> style="color: white;" class="btn btn-primary btn-block">go to checkout</button>
                    </div>
                </div>
                <!-- Card -->
                <!-- Card -->
                <div class="mb-3">
                    <div class="pt-4">
                        <div class="collapse" id="collapseExample">
                            <div class="mt-3">
                                <div class="md-form md-outline mb-0">
                                    <input type="text" id="discount-code" class="form-control font-weight-light" placeholder="Enter discount code">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {
        $("[type=checkbox]").click(function() {
            let split  = $(this).data('input').split('|')
            let change = $(this).is(':checked') === true ? 1 : 0
            if ($(this).is(':checked') == true) {
                $(this).parent().css({
                    "background-color": "#CCC"
                });
                let data = getdata(split[0], split[1], split[2])
            } else if ($(this).is(':checked') == false) {
                $(this).parent().css({
                    "background-color": "#FFF"
                });
            }

            $.ajax({
                url: '<?php echo base_url('ajax/unable/'); ?>' + split[2] + '/' + change,
                dataType: 'json',
                type: "GET",
                quietMillis: 500,
                success: function(value) {
                    console.log(value)
                },
                cache: true,
            })

        });
        $('#gotocheckout').click(function() {
            window.location.href = '<?php echo base_url() . 'member/checkout/' ?>';
        })
        $('[type=button]').click(function() {
            let split  =  $(this).data('remove').split('|')
            $.ajax({
                url: '<?php echo base_url('ajax/deletecart/'); ?>' + split[2] + '/' + split[0] + '/' + split[1],
                dataType: 'json',
                type: "GET",
                quietMillis: 500,
                success: function(value) {
                    window.location.replace("<?= site_url() ?>member/keranjang/addTocart");
                   
                },
                cache: true,
            })
            // window.location.reload(true);
        })
    });

    function getdata(params, middle, end) {
        return $.ajax({
            url: '<?php echo base_url('ajax/updated/'); ?>' + params + '/' + middle + '/' + end,
            // dataType: 'json',
            type: "GET",
            quietMillis: 500,
            success: function(data) {
                return data;
            },
            cache: true,
        })
    }
</script>
<!--Section: Block Content-->