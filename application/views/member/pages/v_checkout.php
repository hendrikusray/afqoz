<div class="container ">
	<h5>Checkout</h5>
	<form method="post" action="http://futuristic.web.id/belanja/simpan">

		<i class="fa fa-map-marker" aria-hidden="true"> Tujuan Pengiriman</i>

		<div class="row pt-3 pb-3 shadow-sm p-3 mb-5 bg-light rounded">
			<div class="col-md-6">
				<div class="form-group ">
					<label>Nama Penerima</label>
					<input class="form-control" type="text" id="nama_penerima" name="nama_penerima" value="<?php echo $users->nama_member; ?>">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group ">
					<label>No Telepon</label>
					<input class="form-control" type="text" onkeypress='validate(event)' id="no_tlp" name="no_tlp" value="<?php echo $users->telp; ?>">
				</div>
			</div>



			<div class="col-md-6">
				<div class="form-group">
					<label>Provinsi</label>
					<select name="provinsi" id="province" class="form-control" required="">
						<option value="">-- Pilih Provinsi --</option>
						<?php foreach ($provinces as $red) { ?>
							<option value="<?php echo $red['province']; ?>" data-province="<?php echo $red['province_id']; ?>|<?php echo $red['province']; ?>"><?php echo $red['province']; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label>Kabupaten/Kota</label>
					<select disabled name="kota" id="kota" class="form-control" required="">
						<option value="">-- Pilih Kota --</option>
					</select>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Alamat Lengkap</label>
					<textarea class="form-control" id="alamat_lengkap" rows="3" cols="50" name="alamat_lengkap" required=""><?php echo $users->alamat; ?></textarea>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group ">
					<label>Kode Pos</label>
					<input class="form-control" type="number" id="kode_pos" name="kode_pos" placeholder="Kode Pos" value="<?php echo $users->kode_pos; ?>" required="">
				</div>
			</div>
		</div>


		<div class="row mt-1 pt-4 shadow-sm p-3 mb-5 bg-light rounded">
			<div class="col-12">
				<table id="mprDetailDataTable" class="table table-responsive">
					<tbody>
						<tr>
							<th>No</th>
							<th>Produk</th>
							<th></th>
							<th>Ukuran</th>
							<th>Jumlah</th>
							<th>Harga</th>
							<th>Subtotal</th>
						</tr>
						<?php $no = 0; ?>
						<?php foreach ($cart as $row) { ?>
							<?php $no++; ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td width="5%">
									<?php foreach (json_decode($row['foto_produk']) as $key => $v) : ?>
										<div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
											<img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" class="d-block w-100 boradius" alt="...">
										</div>
									<?php endforeach ?>
								</td>
								<td><?php echo $row['nama_produk']; ?><input type="hidden" value="78" name="id"></td>
								<td><?php echo $row['ukuran']; ?></td>
								<td align="center"><?php echo $row['qty']; ?></td>
								<td>Rp. <?php echo $row['harga_stok']; ?></td>
								<td>Rp. <?php echo $row['total_harga']; ?></td>
							</tr>
						<?php } ?>
						<tr>
							<th colspan="1">Total</th>
							<td colspan="6" align="right">Rp. <?php echo $total->total ?> <input value="200000" name="total" type="hidden"></td>
						</tr>
					</tbody>
				</table>

				<!-- ---- Save Hidden --- -->
				<input value="500" name="tot_berat" type="hidden">
				<input value="20201225GNEQTPHM" name="kode_pesan" type="hidden">
				<!--<input name="estimasi" type="hidden">-->
				<input name="ongkir" type="hidden" value="11000">
				<input name="totalbayar" type="hidden" value="211000">
				<input name="paket" type="hidden" value="">

			</div>
		</div>


		<div class="row">
			<!-- <h5>Metode Pembayaran</h5> -->
			<div class="col-md-5">

				<div class="form-group">
					<label>Opsi Paket</label>
					<select disabled id="paket" name="paket" class="form-control" required="">
						<option value="">-- Pilih Paket --</option>
						<option value="jne" data-paket="jne">JNE</option>
						<option value="pos" data-paket="pos">POS</option>
						<option value="tiki" data-paket="tiki">TIKI</option>
					</select>
				</div>
				<div class="form-group">
					<label>Service Paket</label>
					<select disabled id="servicepaket" name="servicepaket" class="form-control" required="">
						<option value="">-- Pilih Service --</option>
					</select>
				</div>


			</div>
			<p id="provincevalue" style=" visibility: hidden;"></p>
			<p id="kabupatenvalue" style=" visibility: hidden;"></p>
			<p id="opsipaket" style=" visibility: hidden;"></p>
			<p id="servicepaketvalue" style=" visibility: hidden;"></p>
			<p id="estimasipaket" style=" visibility: hidden;"></p>
			<div class="col-md-2"></div>
			<form id="payment-form" method="post" action="<?= site_url() ?>midtrans/snap/finish">
				<div class="col-md-5">
					<table class="table">
						<tbody>
							<tr>
								<th>Subtotal Harga :</th>
								<td>Rp. <?php echo $total->total ?></td>
							</tr>
							<tr>
								<th>Ongkos Kirim : </th>
								<td>
									<p id="ongkoskirim">Rp. 0</p>
								</td>
							</tr>
							<tr>
								<th>Total Pembayaran : </th>
								<th>
									<p id="totalpembayaran" data-total="<?php echo $total->total ?>">Rp. <?php echo $total->total ?></p>
									<p id="totalall" style=" visibility: hidden;"></p>
								</th>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
			<div class="d-flex flex-column bd-highlight mb-3" style="margin-top: 1px;">
				<div class="p-2"><button disabled class="btn btn-sm btn-primary" id="pay-button" style="color: white;" type="submit" value="submit">LANJUTKAN PEMBAYARAN</button></div>
				<div class="p-2"></div>
				<div class="p-2"></div>
				<div class="p-2"></div>
				<div class="p-2"></div>
			</div>

		</div>
	</form>
</div>
<script>
	$(document).ready(function() {
		$('#province').click(function() {
			let province = $('option:selected', this).data("province").split("|");
			$("#provincevalue").val(province[1]);
			if (province !== undefined) {
				let data = $.ajax({
					url: '<?php echo base_url('ajax/getCity/'); ?>' + province[0],
					dataType: 'json',
					type: "GET",
					quietMillis: 500,
					success: function(value) {
						$("#kota").empty();
						$("#kota").append("<option>-- Pilih Kota --</option>");
						value.data.forEach(function(type) {
							$("#kota").append("<option value=" + type.city_id + " data-city=" + type.city_id + "|" + type.city_name + ">" + type.city_name + "</option>");
						});
						$("#kota").prop('disabled', false);
					},
					cache: true,
				})
			}
		})
		$('#kota').click(function() {
			$("#paket").prop('disabled', false);
		})
		$('#paket').click(function() {
			let city = $('select#kota').find(':selected').data('city').split('|');
			$("#kabupatenvalue").val(city[1]);
			let value = $('option:selected', this).data("paket")
			$("#opsipaket").val(value);
			if (city !== undefined && value !== undefined) {
				let data = $.ajax({
					url: '<?php echo base_url('ajax/getPaket/'); ?>' + city[0] + '/' + value,
					dataType: 'json',
					type: "GET",
					quietMillis: 500,
					success: function(value) {
						$("#servicepaket").empty();
						$("#servicepaket").append("<option>-- Pilih Service --</option>");
						value.data.forEach(function(type) {
							$("#servicepaket").append("<option data-service='" + type.service + "|" + type.cost[0].value + "|" + type.cost[0].etd + "' value=" + type.service + " >" + type.service + " | " + type.cost[0].value + " | " + type.cost[0].etd + " hari</option>");
						});
						$("#servicepaket").prop('disabled', false);
					},
					cache: true,
				})
			}
		})
	});
	$('#servicepaket').click(function() {
		if ($('select#servicepaket').find(':selected').data('service') !== undefined) {
			let services = $('select#servicepaket').find(':selected').data('service')
			let split_services = services.split("|")
			$("#servicepaketvalue").val(split_services[0]);
			$("#estimasipaket").val(split_services[2]);
			$("#ongkoskirim").text("Rp. " + split_services[1] + "");
			let total = parseInt($("#totalpembayaran").attr("data-total")) + parseInt(split_services[1])

			$("#totalpembayaran").text("Rp. " + total + "");
			$('#pay-button').prop('disabled', false);
			$("#totalpembayaran").val(split_services[1]);
			$("#totalall").val(total);
		}
	})
	$('#pay-button').click(function(event) {
		event.preventDefault();
		$(this).attr("disabled", "disabled");
		let ongkir = $('#totalpembayaran').val()
		let total = $('#totalall').val()
		let data_payload_transaction = {
			province: $('#provincevalue').val(),
			city: $('#kabupatenvalue').val(),
			package: $('#opsipaket').val(),
			service: $('#servicepaketvalue').val(),
			estimate: $('#estimasipaket').val(),
		}

		let full_address = $('#alamat_lengkap').val() === '' ? '<?php echo $users->alamat; ?>' : $('#alamat_lengkap').val()
		let nama = $('#nama_penerima').val() === '' ? '<?php echo $users->nama_member; ?>' : $('#nama_penerima').val()
		let telp = $('#no_tlp').val() === '' ? '<?php echo $users->telp; ?>' : $('#no_tlp').val()
		let kode_pos = $('#kode_pos').val() === '' ? '<?php echo $users->kode_pos; ?>' : $('#kode_pos').val()
		let users = {
			full_address: full_address,
			nama: nama,
			telp: telp,
			post_code: kode_pos,
			email: '<?php echo $users->email; ?>'
		}

		console.log(users)


		let cartArray = <?php echo json_encode($cart); ?>

		$.ajax({
			url: '<?= site_url() ?>midtrans/snap/token',
			type: 'post',
			data: {
				"cart": cartArray,
				"ongkir": ongkir,
				"total": total,
				"users": users
			},
			cache: false,

			success: function(data) {
				//location = data;

				console.log('token = ' + data);

				var resultType = document.getElementById('result-type');
				var resultData = document.getElementById('result-data');

				function changeResult(type, data) {
					$("#result-type").val(type);
					$("#result-data").val(JSON.stringify(data));
					//resultType.innerHTML = type;
					//resultData.innerHTML = JSON.stringify(data);
				}

				snap.pay(data, {
					onSuccess: function(result) {
						changeResult('success', result);
						console.log(result.status_message);
						$("#payment-form").submit();
					},
					onPending: function(result) {
						$.ajax({
							url: '<?= site_url() ?>midtrans/snap/finish',
							type: "POST",
							data: {
								result: result,
								cart: cartArray,
								ongkir: ongkir,
								total: total,
								users: users,
								transaction: data_payload_transaction
							},
							success: function(response) {
								window.location.replace("<?= site_url() ?>member/keranjang/myorder");
								// You will get response from your PHP page (what you echo or print)
							},
							error: function(jqXHR, textStatus, errorThrown) {
								console.log(textStatus, errorThrown);
							}
						});
						changeResult('pending', result);
						console.log(result.status_message);
						$("#payment-form").submit();

					},
					onError: function(result) {
						console.log(result, "ray 3");
						changeResult('error', result);
						console.log(result.status_message);
						$("#payment-form").submit();
					}
				});
			}
		});
	});

	function validate(evt) {
		var theEvent = evt || window.event;

		// Handle paste
		if (theEvent.type === 'paste') {
			key = event.clipboardData.getData('text/plain');
		} else {
			// Handle key press
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode(key);
		}
		var regex = /[0-9]|\./;
		if (!regex.test(key)) {
			theEvent.returnValue = false;
			if (theEvent.preventDefault) theEvent.preventDefault();
		}
	}
</script>