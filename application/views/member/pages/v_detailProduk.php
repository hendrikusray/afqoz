<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Detail Produk</title>


	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,400;0,500;1,400&display=swap" rel="stylesheet">
	<style>
		.hide {
			display: none;
		}

		.navbar-nav>.nav-item {
			text-align: center;
			font-weight: 500;
		}

		#lblCartCount {
			font-size: 12px;
			background: #ff0000;
			color: #fff;
			padding: 0 5px;
			vertical-align: top;
		}

		.txt-kategori {
			font-size: 17px;
			margin-top: -20px;

		}

		.kategori-sepatu {
			height: 200px;
			background-image: url('http://futuristic.web.id/assets/img/zoom1.jpg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: auto 200px;
		}

		.kategori-sale {
			height: 200px;
			background-image: url('http://futuristic.web.id/assets/img/zoom3.jpeg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: auto 200px;
		}

		.kategori-atasan {
			height: 200px;
			background-image: url('http://futuristic.web.id/assets/img/zoom2.jpg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: auto 200px;
		}

		.kategori-sepatu:hover {
			transform: scale(1.1);
			transition: all ease 500ms;
		}

		.kategori-sale:hover {
			transform: scale(1.1);
			transition: all ease 500ms;
		}

		.kategori-atasan:hover {
			transform: scale(1.1);
			transition: all ease 500ms;
		}

		.layer-kategori {
			transition: all 1s;
			background: rgba(0, 0, 0, 0.5);
			width: 100%;
			height: 100%;
			color: white;
			position: absolute;
			top: 0;
			left: 0;
			padding: 5rem 0;
		}

		.bg-gray {
			background: gray;
		}

		.boradius {

			border-radius: 5px;
			border: 2px solid grey;
		}


		/* on desktop */
		@media (min-width: 1000px) {
			.scroller {
				width: 100%;
				border: 0px solid transparent;
				display: flex;
				overflow-x: auto;

			}

			.card-medi {
				width: 200px;
				height: 200px;
				line-height: 100px;
				text-align: center;
				margin-right: 25px;

			}

		}


		/* === on mobile == */

		@media (max-width: 768px) {
			#sliderds {
				width: 100%;
			}

			.scroller {
				max-height: 200px;
				border: 0px solid transparent;
				display: flex;
				overflow-x: auto;
			}

			.card-medi {
				min-width: 150px;
				height: 150px;
				line-height: 75px;
				text-align: center;
				background-color: #ddd;
				margin-right: 10px;
			}

			.fokat {
				width: 140px;
			}

			.errorTxt {
				border: 1px solid red;
				min-height: 20px;
			}

		}
	</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<?php
			foreach ($details as $i) :
				$id_produk = $i->id_produk;
				$nama_produk = $i->nama_produk;
				$id_kategori = $i->id_kategori;
				$foto_produk = json_decode($i->foto_produk);
				// $harga = $i->harga;
				// $diskon = $i->diskon;
				$deskripsi = $i->deskripsi;
				$berat_produk = $i->berat_produk;
			?>

				<!-- <div class="row pt-3"> -->
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<?php foreach ($foto_produk as $key => $v) : ?>
								<div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
									<img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" class="d-block w-100 boradius" alt="...">
								</div>
							<?php endforeach ?>
						</div>
						<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					<div class="">
					<form id="form" action="<?php echo base_url() . 'member/keranjang/addTocart/'.$id_produk ?>" method="post">
						<p class="card-title mb-1 font-weight-bold" style="font-size: 24px;">
							<?php echo $details[0]->nama_produk; ?></p>

						<p class="text-muted mb-0">
							<!-- <medium class="font-weight-bold">
								<span id="harga"></span>
							</medium> -->
						<h5 class="font-weight-bold" id="harga" style="color:orange;">Rp. 0</h5>
						</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-lg-12 col-md-6 col-xs-12 p-3 " style="background-color: #f39c12; width: 100%;">
						<p class="h5 font-weight-bold mb-2 text-center" style="color: white;">Detil Produk</p>
					</div>


						<div class="box">
							<div class="card">

								<div class="card-body p-3">
									<p class=" mb-0">
										<medium class="font-weight-bold">Detail Produk
											<h6><?php echo $details[0]->deskripsi; ?></h6>
									</p><br>
									<p class=" mb-0">
										<medium class="font-weight-bold">Size Produk
										</medium>
									</p>
									<table class="table">
										<tbody>
											<!-- bagian tombol ukuran stok	 -->
											<tr>
												<td>
													<?php foreach ($details[0]->stok as $dtl) : ?>
														<button class="btn btn-light stok btnstok" data-jumlah_stok="<?php echo $dtl->jumlah_stok ?>|<?php echo $dtl->harga?>|<?php echo $dtl->ukuran_stok?>" <?= ($dtl->jumlah_stok == 0) ? 'disabled' : '' ?>>
															<?php echo $dtl->ukuran_stok ?>
														</button>
													<?php endforeach; ?>
												</td>
											</tr>
											<!-- bagian keterangan juml stok  -->
											<tr>
												<td>
													<span id="jumlah_stok"> </span>
												</td>
											</tr>
												<tr>
													<td>
														<div class="row">
															<div class="input-group" style="width:38%;margin-left:5px">
																<div class="input-group-prepend">
																	<span style=" font-weight: bold;" class="input-group-text" maxlength="4" id="inputGroup-sizing-default">Jumlah</span>
																</div>
																<input disabled type="number" id="qtynumber" name="qtynumber" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
																<span id="errorMsg" style="display:none;"></span>
															</div>
														</div>
														<div class="col-lg-12 col-md-6 col-xs-12  pl-3 pr-3">
															<button type="submit" id="addTocart" disabled class="btn btn-danger btn-block">Tambah ke Keranjang </button>
														</div>
													</td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</form>

				</div>
				<!-- </div> -->
		</div>
	</div>
<?php endforeach; ?>

</body>

<script>
	$(document).on('click', '.btnstok', function(e) {
		e.preventDefault()
		let data   = $(this).data('jumlah_stok').split("|")
		let stok   = data[0]
		let harga  = data[1]
		let ukuran = data[2]
		$("<input />").attr("type", "hidden")
          .attr("name", "ukuran")
          .attr("value", `${ukuran}`)
          .appendTo("#form");

		$("#qtynumber").prop('disabled', false);
		$('.btnstok').removeClass('btn-secondary');
		$(this).addClass('btn-success');

		$('#jumlah_stok').html(stok + " Stok Yang Tersedia")
		$('#jumlah_stok').val(stok)
		$("#qtynumber").attr({
			"max": stok,
			"min": 1
		})

		$('#harga').html("Rp. " + harga)
	})

	$("#qtynumber").keyup(function() {
		console.log($('#jumlah_stok').val())
		if ($('#qtynumber').val() < 0 || $('#qtynumber').val() > $('#jumlah_stok').val()) {
			$('#errorMsg').show();
			$('#errorMsg').html("anda hanya boleh order max " + $('#jumlah_stok').val() + " item")
			$("#addTocart").prop('disabled', true);
		} else {
			$('#errorMsg').hide();
			$("#addTocart").prop('disabled', false);
		}
	});
</script>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>
<!-- Bootstrap core JavaScript-->