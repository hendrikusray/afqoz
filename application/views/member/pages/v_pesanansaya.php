<div class="container-fluid" style="width:90%">
    <h6 class="section-title h3">Belanja Saya</h6>
    <div class="row w-100">
        <div class="col-lg-12">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Belum Bayar</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Pesanan Diantar</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Pesanan Selesai</a>
                </div>
            </nav>
            <div class="tab-content py-3 px-3 px-sm-0" style="background-color: #F6F6F6;" id="nav-tabContent">
                <div class="tab-pane fade show active" style="padding: 12px;" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <?php foreach ($onpayment as $row) { ?>
                        <div class="card" style="width: 50rem;margin-top: 20px;">
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="p-2">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm">
                                                    <h5 class="card-title font-weight-bold">order-<?php echo $row->ordercode; ?></h5>
                                                    <table style="width:60%">
                                                        <tr>
                                                            <td>Alamat</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->alamat; ?>, <?php echo $row->kabupaten; ?>, <?php echo $row->provinsi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Service Paket</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->service_paket; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Penerima</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->nama_penerima; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No Resi</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->no_resi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga Ongkir</td>
                                                            <td>:</td>
                                                            <td>Rp.<?php echo $row->ongkir; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Harga</td>
                                                            <td>:</td>
                                                            <td><b>Rp.<?php echo $row->all_total; ?></b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Virtual Pembayaran</td>
                                                            <td>:</td>
                                                            <td><b><?php echo $row->va; ?></b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status Pemesanan</td>
                                                            <td>:</td>
                                                            <?php if ($row->status_barang === 'expire') : ?>
                                                                <td><b><span class="text-danger">kadaluarsa</span><b /></td>
                                                            <?php elseif ($row->status_barang === 'pending') : ?>
                                                                <td><b><span class="text-warning">pending</span><b /></td>
                                                            <?php endif; ?>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <button type="button" style="color: white;" class="btn btn-primary" data-toggle="modal" data-target="#onpayment<?php echo $row->id; ?>">
                                            Detail Pemesanan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="nav-profile" style="padding: 12px;" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <?php foreach ($ongoing as $row) { ?>
                        <div class="card" style="width: 50rem;margin-top: 20px;">
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="p-2">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm">
                                                    <h5 class="card-title font-weight-bold">order-<?php echo $row->ordercode; ?></h5>
                                                    <table style="width:60%">
                                                        <tr>
                                                            <td>Alamat</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->alamat; ?>, <?php echo $row->kabupaten; ?>, <?php echo $row->provinsi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Service Paket</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->service_paket; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Penerima</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->nama_penerima; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No Resi</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->no_resi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga Ongkir</td>
                                                            <td>:</td>
                                                            <td>Rp.<?php echo $row->ongkir; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Harga</td>
                                                            <td>:</td>
                                                            <td><b>Rp.<?php echo $row->all_total; ?></b></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <button type="button" style="color: white;" class="btn btn-primary" data-toggle="modal" data-target="#ongoing<?php echo $row->id; ?>">
                                            Detail Pemesanan
                                        </button>
                                        <button type="button" style="color: white;" id="clearpemesanan" class="btn btn-success" onclick="showdata(<?php echo $row->id ?>)" data-idorders="<?php echo $row->id ?>">
                                            Sudah Sampai
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="nav-contact" style="padding: 12px;" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <?php foreach ($finish as $row) { ?>
                        <div class="card" style="width: 50rem;margin-top: 20px;">
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="p-2">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm">
                                                    <h5 class="card-title font-weight-bold">order-<?php echo $row->ordercode; ?></h5>
                                                    <table style="width:60%">
                                                        <tr>
                                                            <td>Alamat</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->alamat; ?>, <?php echo $row->kabupaten; ?>, <?php echo $row->provinsi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Service Paket</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->service_paket; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Penerima</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->nama_penerima; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No Resi</td>
                                                            <td>:</td>
                                                            <td><?php echo $row->no_resi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga Ongkir</td>
                                                            <td>:</td>
                                                            <td>Rp.<?php echo $row->ongkir; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Harga</td>
                                                            <td>:</td>
                                                            <td><b>Rp.<?php echo $row->all_total; ?></b></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <button type="button" style="color: white;" class="btn btn-primary" data-toggle="modal" data-target="#finish<?php echo $row->id; ?>">
                                            Detail Pemesanan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
foreach ($onpayment as $red) :
    $id     = $red->id;
    $produk = $red->produk;
?>
    <div class="modal fade bd-example-modal-lg" id="onpayment<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="container">
                                    <?php foreach ($produk as $row) { ?>
                                        <div class="row">
                                            <div class="card bg-light mb-3" style="min-width: 30rem;">
                                                <div class="card-header"><?php echo $row->nama_produk ?></div>
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-8">
                                                                <h5 class="card-title">
                                                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                                        <div class="carousel-inner">
                                                                            <?php foreach (json_decode($row->foto_produk) as $key => $v) : ?>
                                                                                <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                                                                    <img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" style="width: 50%;" class="d-block boradius" alt="...">
                                                                                </div>
                                                                            <?php endforeach ?>
                                                                        </div>
                                                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                </h5>
                                                            </div>
                                                            <div class="col-4">
                                                                <p><?php echo $row->deskripsi ?></p>
                                                                <p>Rp.<?php echo $row->harga ?></p>
                                                                <p>Qty : <?php echo $row->qty ?></p>
                                                                <p>ukuran : <?php echo $row->ukuran ?></p>
                                                                <p>Total : Rp.<?php echo $row->harga * $row->qty ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="color:white" data-dismiss="modal" class="btn btn-primary">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php endforeach; ?>
<!-- .onprohress -->
<?php
foreach ($ongoing as $red) :
    $id     = $red->id;
    $produk = $red->produk;
?>
    <div class="modal fade bd-example-modal-lg" id="ongoing<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="container">
                                    <?php foreach ($produk as $row) { ?>
                                        <div class="row">
                                            <div class="card bg-light mb-3" style="min-width: 30rem;">
                                                <div class="card-header"><?php echo $row->nama_produk ?></div>
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-8">
                                                                <h5 class="card-title">
                                                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                                        <div class="carousel-inner">
                                                                            <?php foreach (json_decode($row->foto_produk) as $key => $v) : ?>
                                                                                <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                                                                    <img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" style="width: 50%;" class="d-block boradius" alt="...">
                                                                                </div>
                                                                            <?php endforeach ?>
                                                                        </div>
                                                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                </h5>
                                                            </div>
                                                            <div class="col-4">
                                                                <p><?php echo $row->deskripsi ?></p>
                                                                <p>Rp.<?php echo $row->harga ?></p>
                                                                <p>ukuran : <?php echo $row->ukuran ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="color:white" data-dismiss="modal" class="btn btn-primary">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php endforeach; ?>
<!-- finish -->
<?php
foreach ($finish as $red) :
    $id     = $red->id;
    $produk = $red->produk;
?>
    <div class="modal fade bd-example-modal-lg" id="finish<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="container">
                                    <?php foreach ($produk as $row) { ?>
                                        <div class="row">
                                            <div class="card bg-light mb-3" style="min-width: 30rem;">
                                                <div class="card-header"><?php echo $row->nama_produk ?></div>
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-8">
                                                                <h5 class="card-title">
                                                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                                        <div class="carousel-inner">
                                                                            <?php foreach (json_decode($row->foto_produk) as $key => $v) : ?>
                                                                                <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                                                                    <img src="<?php echo base_url() . 'uploads/produk/' . $v ?>" style="width: 50%;" class="d-block boradius" alt="...">
                                                                                </div>
                                                                            <?php endforeach ?>
                                                                        </div>
                                                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                </h5>
                                                            </div>
                                                            <div class="col-4">
                                                                <p><?php echo $row->deskripsi ?></p>
                                                                <p>Rp.<?php echo $row->harga ?></p>
                                                                <p>ukuran : <?php echo $row->ukuran ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="color:white" data-dismiss="modal" class="btn btn-primary">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php endforeach; ?>
<script>
    $(document).ready(function() {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            } else
                localStorage.removeItem('firstLoad');
        }
    });

    function showdata(elem) {
        // console.log(elem)
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Apakah Barang Anda Sudah Sampai?',
            text: "Jika Sudah Sampai harap konfirmasi dengan button Yes, Jika tidak tekan No!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?php echo base_url('ajax/updateStatusPembayaran/'); ?>' + elem,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 500,
                    success: function(value) {
                        swalWithBootstrapButtons.fire(
                            'Thank You!',
                            'Terimakasih sudah memesan',
                            'success'
                        ).then(function() {
                            location.reload();
                        })

                    },
                    cache: true,
                })

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>