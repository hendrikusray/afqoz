<div class="container">
	<div class="card w-75">
		<div class="card-body">
			<h5 class="card-title">Profil Saya</h5>
			<hr />
			<div class="col-md-2"></div>
			<div class="row">
				<div class="col-lg-6">
				</div>
			</div>
			<p class="card-text">
			</p>
			<?php
			if ($msg === 'success') {
				echo '<div class="alert alert-success" role="alert">
						update data member berhasil
						</div>';
			} else if ($msg === 'failed') {
				echo '<div class="alert alert-danger" role="alert">
						gagal update data member
						</div>';
			}
			?>
			<form action="<?php echo base_url() . 'member/akunku/updateDataMember' ?>" method="post">
				<table class="table table-borderless">
					<tbody>
						<tr>
							<td>Username </td>
							<td><?php echo $username_member; ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $email; ?></td>
						</tr>
						<form action="<?php echo base_url() . 'member/akunku/updateDataMember' ?>" method="post"></form>
						<tr>
							<td>Nama Pengguna</td>
							<td>
								<input type="text" class="form-control" id="nama_member" name="nama_member" value="<?php echo $nama_member; ?>">
							</td>
						</tr>
						<tr>
							<td>Nomor Telepon</td>
							<td>
								<input onkeypress="return isNumber(event)" type="text" class="form-control" id="tlp" name="tlp" value="<?php echo $telp; ?>">
							</td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td>
								<textarea class="form-control" id="alamat" name="alamat"><?php echo $alamat; ?></textarea>
								<!-- <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $alamat; ?>"> -->
							</td>
						</tr>
						<tr>
							<td>Password</td>
							<td><a href="<?php echo base_url() . 'member/akunku/resetpass' ?>" style="color:white;font-weight: bold;" class="btn btn-sm btn-danger">change password</a></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="form-group row">
									<div class="col-sm-10">
										<button style="color:white" type="submit" class="btn btn-sm btn-primary">UBAH</button>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<script>
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<!-- !! MODAL -->