<section id="slider">
	<div class="container">
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="<?php echo base_url('assets/slider/tkp4.jpg') ?>" class="d-block w-100 boradius">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/slider/tkp-sepatu.jpg.webp') ?>" class="d-block w-100 boradius">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/slider/tkp5.webp') ?>" class="d-block w-100 boradius">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/slider/tkp-tas.webp') ?>" class="d-block w-100 boradius">
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</section> <br>

<section class="container">
	<div class="row">
		<div class="col">
			<header id="search" class="section-header">
				<h6><b>Cari Produk Sesukamu!</b></h6>
			</header>
			<div class="boxContainer">
				<table class="elementContainer">
					<tr>
						<td class="putihtxt">
						<form action="<?php echo site_url('member/beranda"');?>" method="post"> 
							<input type="text" name="keyword_product" placeholder="  Search.." class="search putihtxt">
						</td>
						<td>
							<button type="submit"><i class="material-icons fa fa-search"> </i></button>
							<?php echo form_close() ?>
						</td>
						</form>
					</tr>
				</table>
			</div>
			
		</div>

		
</section>


<main id="main">
	<!-- ======= Produk Section ======= -->
	<section id="produk" class="produk ">
		<div class="container">
			<header class="section-header">
				<h6><b>Produk </b></h6>
				<!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p> -->
			</header>

			<div class="row section-bg">
				<?php
				foreach ($data as $i) :
					$id_produk = $i->id_produk;
					$nama_produk = $i->nama_produk;
					$id_kategori = $i->id_kategori;
					$foto_produk = json_decode($i->foto_produk);
					// $harga = $i->harga;
					// $diskon = $i->diskon;
					$deskripsi = $i->deskripsi;
					$berat_produk = $i->berat_produk;
				?>
					<div class="col-6 col-md-4 col-lg-3  wow bounceInUp " data-wow-duration="1.4s">
						<div class="box">
							<div class="card ">
								<a href="<?= base_url('member/produk/detail_produk/' . $id_produk) ?>">
									<img class="card-img-top fotoproduk" src="<?php echo base_url() . 'uploads/produk/' . $foto_produk[0]; ?>" alt="Foto Produk">
									<div class="card-body p-3">
										<h5 class="card-title mb-1 font-weight-bold" style="font-size: 14px;"><b>
												<?php echo word_limiter($nama_produk, 10); ?></b></h5>
									
										<button type="button" class="btn btn-sm btn-outline-info">Pilih Produk</button>
									</div>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<!-- End Produk Section -->


</main><!-- End #main -->

<br>