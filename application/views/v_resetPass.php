<div class="row justify-content-center">
  <div class="col-lg-5">
    <div class="card o-hidden border-1 ">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Forgot Your Password</h1>
              </div>
              <?php
                    if($msg === 'null') {
                    echo '<div class="alert alert-danger" role="alert">
                      email anda belum terdaftar
                    </div>'; 
                    } else if($msg === 'success') {
                      echo '<div class="alert alert-success" role="alert">
                      silahkan check email anda!  
                    </div>'; 
                    } else if($msg === 'email_not_send') {
                      echo '<div class="alert alert-success" role="alert">
                     gagal mengirimkan email. coba lagi!  
                    </div>'; 
                    }
              ?>
             
              <form action="<?php echo base_url().'forgotpass/forgot_password'?>" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" id="text" name="email_reset" placeholder="Enter Email..." value="">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-dark btn-block p-2">
                    <span style="color: white;">Reset Password</span>
                  </button>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="<?php echo base_url() . 'login' ?>">Back To Login</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>