<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

class rajaOngkir {
    private $ci;
    private $KEY      = 'ea4f4df910a21ae57e0fd5fed3f7d881';
    private $URL      = 'https://api.rajaongkir.com/starter';

    public function __construct() {
        $this->ci   = & get_instance();

        log_message('debug', 'GoApotik Services initialized');
    }

    public function get($url, $header = []) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $response   = curl_exec($ch);
        $response   = json_decode($response, TRUE);
        curl_close($ch);

        return $response;
    }

    public function post($url, $header = [], $payload = []) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $response   = curl_exec($ch);
        $response   = json_decode($response, TRUE);
        curl_close($ch);

        return $response;
    }

    public function getProvince() {
        $full_url = $this->URL.'/province';
        $response = $this->get($full_url,array('accept:application/json', 'key:'.$this->KEY));
        return $response;
    }

    public function getCity($province_id) {
        $full_url = $this->URL.'/city?province='.$province_id;
        $response = $this->get($full_url,array('accept:application/json', 'key:'.$this->KEY));
        return $response;
    }

    public function getCost($payload  = array()) {
        $full_url = $this->URL.'/cost';
        $response = $this->post($full_url,array('accept:application/json', 'key:'.$this->KEY),$payload);
        return $response;
    }
}