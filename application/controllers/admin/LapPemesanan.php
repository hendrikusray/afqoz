<?php
class LapPemesanan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('m_transaction');
    }

    function index()
    {
        $start_date = $this->input->get('dates', TRUE);;
        $end_date   = $this->input->get('datend', TRUE);

        if($start_date === NULL && $end_date === NULL) {
            $x['data'] = $this->m_transaction->report_transaction_function('(pembayaran.status_pemesanan = 1 OR pembayaran.status_pemesanan = 2)');
    
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_lapPemesanan',$x);
            $this->load->view('admin/templates/footer');
        } else {
            $payload       = array(
                'pembayaran.created_at >=' => $start_date,
                'pembayaran.created_at <=' => $end_date
             );
            $x['data']    = $this->m_transaction->report_transaction_function($payload);
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_lapPemesanan',$x);
            $this->load->view('admin/templates/footer');
        }
       
    }
    

}
