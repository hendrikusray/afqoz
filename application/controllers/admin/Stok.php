<?php
class Stok extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->Model('M_Stok');
    }
    function index(){
        $start_date = $this->input->get('dates', TRUE);;
        $end_date   = $this->input->get('datend', TRUE);

        if($start_date === NULL && $end_date === NULL) {
            $x['data'] = $this->M_Stok->get_datastok();
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_stok', $x);
            $this->load->view('admin/templates/footer');
        } else {
            $payload       = array(
                'produk.created_at >=' => $start_date,
                'produk.created_at <=' => $end_date
            );
            $x['data'] = $this->M_Stok->get_datastok_where($payload);
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_stok', $x);
            $this->load->view('admin/templates/footer');
        }
    }  


    function save($id) 
    {
        if($this->M_Stok->input_data($id)){
            echo 'berhasil';
        }

    
    }

    public function show($id_produk)
    {
        echo json_encode($this->M_Stok->getOneStokByIdProduk($id_produk));
    }


    
}

?>