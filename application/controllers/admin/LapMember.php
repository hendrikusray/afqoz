<?php
class LapMember extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('m_transaction');
        $this->load->model('m_laporan');
    }

    function index()
    {
        $start_date = $this->input->get('dates', TRUE);;
        $end_date   = $this->input->get('datend', TRUE);

        if($start_date === NULL && $end_date === NULL) {
            $x['data'] = $this->m_laporan->get_member();
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_lapMember', $x);
            $this->load->view('admin/templates/footer');
        } else {
            $payload       = array(
                'member.created_at >=' => $start_date,
                'member.created_at <=' => $end_date
            );
            $x['data'] = $this->m_laporan->get_member_condition($payload);
            $this->load->view('admin/templates/header');
            $this->load->view('admin/v_lapMember', $x);
            $this->load->view('admin/templates/footer');
        }
    }
    

}
