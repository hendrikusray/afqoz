<?php
class Produk extends CI_Controller
{
    function __construct()
    {
        
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
   
        $this->load->model('m_produk');
    }

    function index()
    {
        $x['combo'] = $this->m_produk->get_kategori()->result();
        $x['data'] = $this->m_produk->get_produk();
        // $x['datastok'] = $this->m_produk->get_datastok($id_produk);
        $this->load->view('admin/templates/header');
        $this->load->view('admin/v_produk', $x);
        $this->load->view('admin/templates/footer');
    }
    
    function save()
    {
        $id_kategori = strip_tags($this->input->post('id_kategori'));
        $foto_produk = strip_tags($this->input->post('foto_produk'));
        // $harga = strip_tags($this->input->post('harga'));
        // $diskon = strip_tags($this->input->post('diskon'));
        $deskripsi = strip_tags($this->input->post('deskripsi'));
        $berat_produk = strip_tags($this->input->post('berat_produk'));
        $status = false;
        
        $tmpng_foto = [];

        if (!empty($_FILES['foto_produk'])) {

            foreach ($_FILES['foto_produk']['name'] as $key => $foto){
                
                $_FILES['file']['name'] = $_FILES['foto_produk']['name'][$key];
				$_FILES['file']['type'] = $_FILES['foto_produk']['type'][$key];
				$_FILES['file']['tmp_name'] = $_FILES['foto_produk']['tmp_name'][$key];
				$_FILES['file']['error'] = $_FILES['foto_produk']['error'][$key];
				$_FILES['file']['size'] = $_FILES['foto_produk']['size'][$key];
                $config['upload_path'] = './uploads/produk/'; //path folder
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
                $config['encrypt_name'] = FALSE; //nama yang terupload nantinya
                $config['max_size']     = 3024; // 3MB
                $changeName= "Produk_".date("Y_m_d_").time().".".strtolower(pathinfo($foto, PATHINFO_EXTENSION));
                $config['file_name'] = $changeName;
    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
    
                if ($this->upload->do_upload('file')) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/images/' . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['quality'] = '60%';
                    $config['width'] = 710;
                    $config['height'] = 420;
                    $config['new_image'] = './assets/images/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    array_push($tmpng_foto, $gbr['file_name']);
                } else {
                    echo " <script>
                            alert('Error !!!! Data Produk Gagal ditambahkan');
                            window.location='" . site_url('admin/produk/') . "';
                    </script>";
                    exit();
                }
    
            }
            $nama_produk = strip_tags($this->input->post('nama_produk'));
            $id_kategori = strip_tags($this->input->post('id_kategori'));
            // $harga = strip_tags($this->input->post('harga'));
            // $diskon = strip_tags($this->input->post('diskon'));
            $deskripsi = strip_tags($this->input->post('deskripsi'));
            $berat_produk = strip_tags($this->input->post('berat_produk'));
            

            $data = array(
                'nama_produk' => $nama_produk,
                'id_kategori' => $id_kategori,
                'foto_produk' => json_encode($tmpng_foto),
                // 'harga'       => $harga,
                // 'diskon'      => $diskon,
                'deskripsi'   => $deskripsi,
                'berat_produk'=> $berat_produk,
                'status'      => $status
            );
            $this->m_produk->input_data($data, 'produk');
            echo " <script>
                            alert('Data Produk Berhasil ditambahkan');
                            window.location='" . site_url('admin/produk/') . "';                                
                            </script>";
            exit();
            
        } else {
            echo " <script>
                        alert('Error !!!! Ada Data yang belum ditambahkan');
                        window.location='" . site_url('admin/produk') . "';
                </script>";
        }
    }

    function delete($id)  //delete produk
    {
        $where = array('id_produk' => $id);
        $this->m_produk->delete_data($where, 'produk');
        redirect('admin/produk');
    }
   
    function apdet() //update
    {
        $where = array(
            'id_produk' => $this->input->post('id_produk'),
        );

        $nama_produk = strip_tags($this->input->post('nama_produk'));
        $id_kategori = strip_tags($this->input->post('id_kategori'));
        $foto_produk = strip_tags($this->input->post('foto_produk'));
        // $harga = strip_tags($this->input->post('harga'));
        // $diskon = strip_tags($this->input->post('diskon'));
        $deskripsi = strip_tags($this->input->post('deskripsi'));
        $berat_produk = strip_tags($this->input->post('berat_produk'));
        
        $data = array(
            'nama_produk' => $nama_produk,
            'id_kategori' => $id_kategori,
            // 'harga'       => $harga,
            // 'diskon'      => $diskon,
            'deskripsi'   => $deskripsi,
            'berat_produk'=> $berat_produk
        );

        if (!empty($_FILES['foto_produk'])) {
            if($_FILES['foto_produk']['name'][0] != ""){
                $tmpng_foto = [];
                foreach ($_FILES['foto_produk']['name'] as $key => $foto){
                    $_FILES['file']['name'] = $_FILES['foto_produk']['name'][$key];
                    $_FILES['file']['type'] = $_FILES['foto_produk']['type'][$key];
                    $_FILES['file']['tmp_name'] = $_FILES['foto_produk']['tmp_name'][$key];
                    $_FILES['file']['error'] = $_FILES['foto_produk']['error'][$key];
                    $_FILES['file']['size'] = $_FILES['foto_produk']['size'][$key];
                    $config['upload_path'] = './uploads/produk/'; //path folder
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
                    $config['encrypt_name'] = FALSE; //nama yang terupload nantinya
                    $config['max_size']     = 3024; // 3MB
                    $changeName= "Produk_".date("Y_m_d_").time().".".strtolower(pathinfo($foto, PATHINFO_EXTENSION));
                    $config['file_name'] = $changeName;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('file')) {
                        $gbr = $this->upload->data();
                        //Compress Image
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './assets/images/' . $gbr['file_name'];
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = FALSE;
                        $config['quality'] = '60%';
                        $config['width'] = 710;
                        $config['height'] = 420;
                        $config['new_image'] = './assets/images/' . $gbr['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

                        array_push($tmpng_foto, $gbr['file_name']);
                    } else {
                        echo " <script>
                                alert('Error !!!! Data Produk Gagal ditambahkan');
                                window.location='" . site_url('admin/produk/') . "';
                        </script>";
                        exit();
                    }
                }

                $data['foto_produk'] = json_encode($tmpng_foto);
            }
        }

        $this->m_produk->update_data($where, $data, 'produk');

        redirect('admin/produk');
    }

    public function addsize($id_produk){ 
        $result = $this->m_produk->tambahSize($id_produk);
        if($result === true) {
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Data Ukuran Berhasil Ditambahkan</div>');
        } else {
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed to Update</div>');
        }
        
        redirect('admin/produk/index');
    }

    public function getOneProduk($id_produk)
    {
        $produk = $this->m_produk->show($id_produk)[0];
        $sql = "SELECT * FROM kategori WHERE id_kategori='$produk->id_kategori'";
        $query = $this->db->query($sql);
        $produk->id_kategori = $query->result()[0];
        
        echo json_encode($produk);
    }

    public function getProdukWithStok()
    {
        echo json_encode($this->m_produk->withStok());
    }
}