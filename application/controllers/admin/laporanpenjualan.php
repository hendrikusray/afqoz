<?php
class laporanpenjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('m_transaction');
    }

    function index()
    {
        $x['data'] = $this->m_transaction->report_transaction();
        // var_dump($x);
        $this->load->view('admin/templates/header');
        $this->load->view('admin/v_transaksi', $x);
        $this->load->view('admin/templates/footer');
    }

    function update($id = null)
    {
        if ($id != NULL) {
            $data         = array();
            if($this->input->post("opsibarangstatus") != NULL) {
                $data['status_barang'] = $this->input->post("opsibarangstatus");
            }

            if($this->input->post("optionstatus") != NULL) {
                $data['status_pemesanan'] = $this->input->post("optionstatus");
                if($this->input->post("optionstatus") == 1) {
                    $data['status_barang'] = 'settlement';
                }
            }

            $this->m_transaction->update_data(array('id'=>$id),$data,'pembayaran');
            echo " <script>
            alert('Data updated');
            window.location='" . site_url('admin/laporanpenjualan') . "';                                
            </script>";
            exit();
        } else {
            echo " <script>
                        alert('data failed to update');
                        window.location='" . site_url('admin/laporanpenjualan') . "';
                </script>";
        }
    }
}
