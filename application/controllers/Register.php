<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

use Firebase\JWT\JWT;

class Register extends CI_Controller
{
    private $ci;
    private $ALG    = 'RS256';
    private $KEY    = './application/helpers/jwt-key.pem';

    function __construct()
    {
        parent::__construct();

        $this->load->model('m_register');
        $this->load->model('m_member');
    }
    function index()
    {
        $this->load->view('v_register');
    }


    public function generateToken($email)
    {
        $payload        = array(
            'email'   => $email,
        );
        $secret         = file_get_contents($this->KEY);
        return JWT::encode($payload, $secret, $this->ALG);
    }


    function create_regis()
    {
        $nama_member     = strip_tags($this->input->post('nama_member'));
        $username_member = strip_tags($this->input->post('username_member'));
        $email           = strip_tags($this->input->post('email'));
        $password_member = md5(strip_tags($this->input->post('password_member')));
        $alamat          = strip_tags($this->input->post('alamat'));
        $status          = 0;
        $token           = $this->generateToken($email);

        $check = $this->m_member->get_member_email($email);

        if ($check !== NULL) {
            echo "<script>
            alert('Email telah terdaftar');
            window.location='" . site_url('register') . "';
         </script>";
        } else {
            $data = array(
                'nama_member'     => $nama_member,
                'username_member' => $username_member,
                'email'           => $email,
                'password_member' => $password_member,
                'alamat'     => $alamat,
                'token'      => $token,
                'status'     => $status
            );
            $this->m_register->input_data($data, 'member');
            $config = [
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'protocol'  => 'ssmtp',
                'smtp_host' => 'ssl://ssmtp.googlemail.com',
                'smtp_user' => 'eproc.konsultasi@gmail.com',
                'smtp_pass'   => 'ymzdpgcrtifeiqid',
                'smtp_crypto' => 'ssl',
                'smtp_port'   => 465,
                'crlf'    => "\r\n",
                'newline' => "\r\n"
            ];

            $member  = $this->m_member->get_member_email($email);
            $message = $this->load->view('email/send_email_registration', $member, TRUE);
            $this->load->library('email', $config);
            $this->email->from('eafqoz.ecommerce@gmail.com', 'Aktifasi Akun');
            $this->email->to($email);
            $this->email->subject('Aktivasi Akun');
            $this->email->message($message);

            $send = $this->email->send();
            if ($send) {
                echo " <script>
                alert('Berhasil membuat akun member, silahkan check email untuk aktifasi');
                window.location='" . site_url('login') . "';
              </script>";
            } else {
                echo " <script>
                alert('Berhasil membuat akun member, untuk aktivasi silahkan hubung contact person');
                window.location='" . site_url('login') . "';
              </script>";
            }
        }
    }

     function aktifasiAkun()
    {
        $key = $_GET['q'];
        if (isset($_GET['q'])) {
            $member = $this->m_member->get_token($key);
        }
      
        if ($member !== NULL) {
            $this->m_member->update_data_member(
                array('token' => $key),
                array('status' => 1),
                'member'
            );
            echo " <script>
            alert('Akun member berhasil di aktivasi');
            window.location='" . site_url('login') . "';
          </script>";
        } else {
            echo " <script>
            alert('Akun member tidak berhasil diaktivasi');
            window.location='" . site_url('login') . "';
          </script>";
        }
    }
}
