<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;
class Forgotpass extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('m_member');
    }
    
    function index($arrow = array())
    {
        if (empty($arrow)) {
            $arrow = array(
                'msg' => 'index'
            );
        }

        $this->load->view('member/templates/header');
        $this->load->view('v_resetPass', $arrow);
    }

    function forgot_password()
    {
        $member = array();
        $msg    = array();

        if (isset($_POST['email_reset'])) {
            $member = $this->m_member->get_member_email($_POST['email_reset']);
        }

        if ($member === NULL) {
            $msg =  array(
                'msg' => 'null'
            );
            $this->index($msg);
        } else if (empty($member)) {
            $msg =  array(
                'msg' => 'not_access'
            );
            $this->index($msg);
        } else if (!empty($member)) {
            $msg =  array(
                'msg' => 'success'
            );
            $config = [
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'protocol'  => 'ssmtp',
                'smtp_host' => 'ssl://ssmtp.googlemail.com',
                'smtp_user' => 'eproc.konsultasi@gmail.com',
                'smtp_pass'   => 'ymzdpgcrtifeiqid',
                // 'smtp_crypto' => 'ssl',
                'smtp_port'   => 465,
                'crlf'    => "\r\n",
                'newline' => "\r\n"
            ];

            // $html =  ``
            $message = $this->load->view('email/send_email', $member, TRUE);
            $this->load->library('email', $config);
            $this->email->from('eafqoz.ecommerce@gmail.com', 'Forgot password');
            $this->email->to($member->email);
            $this->email->subject('email forgot password');
            $this->email->message($message);

            $send = $this->email->send();
            if ($send) {
                $this->index($msg);
            } else {
                $this->index(array(
                    'msg' => ' email_not_send'
                ));
            }
        }
    }

    function confirmpass()
    {
        $key = $_GET['q'];
        if (isset($_GET['q'])) {
            $member = $this->m_member->get_token($key);
        }

        if ($member !== NULL) {   
            $this->load->view('member/templates/header');
            $this->load->view('v_forgotPass',$member);
        }
       
    }

    function changePassword()
    {   
        $member   = array();
        $email    = $this->input->post('email_change_pw');
        $token    = $this->input->post('token');
        $password = $this->input->post('new_password');
    
        if ($email != NULL && $token != NULL) {
            $member = $this->m_member->member_with_token($email,$token);
        }
        
        if($member !== NULL && $password !== NULL) {
            $password = md5($password);
            $this->m_member->update_password($email,$token,array('password_member' => $password));
            echo " <script>
            alert('Berhasil mengubah password');
            window.location='" . site_url('login') . "';
        </script>";
        } else {
            echo " <script>
            alert('gagal merubah password');
            window.location='" . site_url('login') . "';
        </script>";
        }

    }
}
