<?php
class Akunku extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('m_member');
    }
    function index()
    {
        $session     = $this->session->all_userdata();
        $member      = $this->m_member->member_with_token($session['email'], $session['token']);
        $member->msg = 'null';
        $this->load->view('member/templates/header');
        $this->load->view('member/pages/v_akunku', $member);
        $this->load->view('member/templates/footer');
    }

    function resetPass()
    {
        // $x['data'] = $this->m_keranjang->get_keranjang();
        $this->load->view('member/templates/header');
        $this->load->view('v_changePass');
        $this->load->view('member/templates/footer');
    }

    function changePass()
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);

        if (!empty($this->input->post('update_password')) && !empty($this->input->post('repeat_update_password')) && $check_user != NULL) {
            $password = md5($this->input->post('update_password'));
            $this->m_member->update_password($session['email'], $session['token'], array('password_member' => $password));
            echo "<script>
                alert('Berhasil mengubah password');
                window.location='" . site_url('member/akunku') . "';
            </script>";
        } else {
            $this->resetPass();
        }
    }

    function updateDataMember()
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        $payload    = array(
            'nama_member' => empty($this->input->post('nama_member')) ? $check_user->nama_member : $this->input->post('nama_member'),
            'telp'        => empty($this->input->post('tlp'))         ? $check_user->telp        : $this->input->post('tlp'),
            'alamat'      => empty($this->input->post('alamat'))      ? $check_user->alamat      : $this->input->post('alamat'),
            'provinsi'    => empty($this->input->post('provinsi'))    ? $check_user->provinsi    : $this->input->post('provinsi'),
            'kecamatan'   => empty($this->input->post('kecamatan'))   ? $check_user->kecamatan   : $this->input->post('kecamatan'),
            'kabupaten'   => empty($this->input->post('kabupaten'))   ? $check_user->kabupaten   : $this->input->post('kabupaten'),
            'kode_pos'    => empty($this->input->post('kode_pos'))    ? $check_user->kode_pos    : $this->input->post('kode_pos'),
        );

        if ($check_user != NULL) {
            $msg     = array();
            $updated = $this->m_member->update_data($session['email'], $session['token'], $payload);
            $member  = $this->m_member->member_with_token($session['email'], $session['token']);
            if ($updated !== NULL) {
                $msg = 'success';
            } else {
                $msg = 'failed';
            }
            $member->msg = $msg;
            $this->load->view('member/templates/header');
            $this->load->view('member/pages/v_akunku', $member);
            $this->load->view('member/templates/footer');
        } else {
            $this->load->view('v_login');
        }
    }
}
