<?php
class Keranjang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('M_Keranjang');
        $this->load->model('m_member');
        $this->load->model('m_produk');
        $this->load->model('m_transaction');
        $this->load->library('veritrans');
        $params = array('server_key' => 'SB-Mid-server-Tr-wluH4NBzKQTBZ0IgyogSd', 'production' => false);
        $this->veritrans->config($params);
    }
    function index($id_member = NULL)
    {
        $data_cart['data'] = $this->M_Keranjang->get_keranjang($id_member);
        $data_cart['count'] = $this->M_Keranjang->count($id_member);

        $this->load->view('member/templates/header');
        $this->load->view('member/pages/v_keranjang', $data_cart);
        $this->load->view('member/templates/footer');
    }

    function addTocart($id_produk = NULL)
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        $produk     = $this->m_produk->find_uk(array('produk.id_produk' => $id_produk, 'stok.ukuran_stok' => $this->input->post('ukuran')));

        if ($check_user != NULL) {
            $auto_selected =  $this->M_Keranjang->find(array(
                'id_member' => $check_user->id_member
            ));

            if (!empty($auto_selected)) {
                $this->M_Keranjang->update_data(array(
                    'id_member' => $check_user->id_member,
                ), array(
                    'is_selected' => 1
                ), 'cart');
            }

            if ($this->input->post('qtynumber') == NULL) {
                $this->index($check_user->id_member);
            } else {
                $find_cart =  $this->M_Keranjang->find(array(
                    'id_member' => $check_user->id_member,
                    'id_produk' => $id_produk,
                    'ukuran'    =>  $this->input->post('ukuran')
                ));

                if (empty($find_cart)) {
                    $payload = array(
                        'id_member'   => $check_user->id_member,
                        'status'      => 0,
                        'id_produk'   => $id_produk,
                        'harga'       => $produk->harga_stok,
                        'total_harga' => (int)$this->input->post('qtynumber') * (int)$produk->harga_stok,
                        'qty'         => $this->input->post('qtynumber'),
                        'nama_produk' => $produk->nama_produk,
                        'is_selected' => 1,
                        'ukuran'      => $this->input->post('ukuran')
                    );

                    $this->M_Keranjang->addtoCart($payload);
                } else {
                    $this->M_Keranjang->update_data(array(
                        'id_member' => $check_user->id_member,
                        'id_produk' => $id_produk
                    ), array(
                        'qty' => (int)$find_cart->qty + (int)$this->input->post('qtynumber'),
                        'total_harga' => ((int)$find_cart->total_harga + ((int)$this->input->post('qtynumber') * (int)$produk->harga_stok))
                    ), 'cart');
                }

                $this->m_produk->update_data(
                    array(
                        'id_produk'     => $id_produk,
                        'ukuran_stok'   => $this->input->post('ukuran')
                    ),
                    array('jumlah_stok' =>  (int)$produk->jumlah_stok - (int)$this->input->post('qtynumber')),
                    'stok'
                );
                $data_cart['data']  =  $this->M_Keranjang->get_keranjang($check_user->id_member);
                $data_cart['count'] = $this->M_Keranjang->count($check_user->id_member);

                $this->load->view('member/templates/header');
                $this->load->view('member/pages/v_keranjang', $data_cart);
                $this->load->view('member/templates/footer');
            }
        } else {
            $this->load->view('v_login');
        }
    }

    function myorder()
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        $data       = array();
      
        if ($check_user != NULL) {
            // $data = $this->m_transaction->get_all_transactions();
            $data = array(
                'onpayment' => $this->m_transaction->get_all_transactions(array('id_member' => $check_user->id_member, 'status_pemesanan' => 0)),
                'ongoing'   => $this->m_transaction->get_all_transactions(array('id_member' => $check_user->id_member, 'status_pemesanan' => 1)),
                'finish'    => $this->m_transaction->get_all_transactions(array('id_member' => $check_user->id_member, 'status_pemesanan' => 2)),
            );
            
            foreach ($data['onpayment'] as $key ) {
                $this->checkorder($key->ordercode);
            }

            
            
            // header("Refresh:0");
            $this->load->view('member/templates/header');
            $this->load->view('member/pages/v_pesanansaya',$data);
            $this->load->view('member/templates/footer');
        } else {
            $this->load->view('v_login');
        }
    }

    
    function checkorder($order = null) {
        if($order != null) {
            $result = $this->veritrans->status($order);
            $status = array();
            // var_dump($result->transaction_status);
            switch ($result->transaction_status) {
                case 'settlement':
                    $status = array('status_barang' => $result->transaction_status,'status_pemesanan' => 1);
                    break;
                
                default:
                    $status = array('status_barang' => $result->transaction_status,'status_pemesanan' => 0);
                    break;
            }
            $this->m_transaction->update_data(array('ordercode' => $order),$status,'pembayaran');
            // 
            return $result;
        }
    }
}
