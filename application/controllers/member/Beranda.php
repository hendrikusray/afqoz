<?php
class Beranda extends CI_Controller
{
        function __construct()
        {
                parent::__construct();
                $this->load->model('m_artikel');
                $this->load->model('m_produk');
                $this->load->helper('text');
                $this->load->helper('date');
        }


        function index()
        {       $keyword = $this->input->post('keyword_product');
                $produk = $this->m_produk->withStok($keyword);
                foreach ($produk as $key => $value) {
                        if(!(count($value->stok) > 0)){
                                unset($produk[$key]);
                        }
                }
                $x['data'] = $produk;
                $x['query'] = $this->m_artikel->get_artikel_page();
                $this->load->view('member/templates/header');
                $this->load->view('member/pages/v_beranda', $x);
                $this->load->view('member/templates/footer');    
                
        }
       
}
