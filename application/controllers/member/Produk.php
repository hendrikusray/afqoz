<?php
class Produk extends CI_Controller
{
        function __construct()
        {
                parent::__construct();

                $this->load->model('m_produk');
                $this->load->model('m_kategori');
                $this->load->helper('text');
                $this->load->helper('date');
        }

        function index()
        {     
                $x['kategori'] = $this->m_kategori->get_kategori();
                if (!isset($_GET['q'])) {
                        $x['data']  = $this->m_produk->get_produk();
                        $x['check'] = 'false';
                        $this->load->view('member/templates/header');
                        $this->load->view('member/pages/v_produk', $x);
                        $this->load->view('member/templates/footer');
                } else {
                        $kategori   =  $this->m_kategori->getbycategoryid($_GET['q']);
                        $x['data']  = $this->m_produk->getproductbykategory($_GET['q']);
                        $x['check'] = $kategori->nama_kategori;
                  
                        $this->load->view('member/templates/header');
                        $this->load->view('member/pages/v_produk', $x);
                        $this->load->view('member/templates/footer');
                }
        }

        function detail_produk($id_produk)
        {
                $x['details'] = $this->m_produk->get_detil_produk($id_produk);
                
                $this->load->view('member/templates/header');
                $this->load->view('member/pages/v_detailProduk', $x);
                $this->load->view('member/templates/footer');
        }

        function keranjang($id_produk) //tambahan
        {
                $x['produk'] = $this->m_produk->find($id_produk);
                $data = array(
                        'id'      => $id_produk,
                        'qty'     => 1,
                        // 'price'   => $harga,
                        // 'name'    => $nama_produk
                );

                $this->cart->insert($data);
                redirect('member/keranjang');
        }

        public function produkWithStok()
        {
                $produk = $this->m_produk->withStok();
                foreach ($produk as $key => $value) {
                        if (!(count($value->stok) > 0)) {
                                unset($produk[$key]);
                        }
                }

                echo json_encode($produk);
        }
}
