<?php
class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        // $this->load->model('m_checkout');
    }
    function index()
    {
        // $x['data'] = $this->m_keranjang->get_keranjang();
        $this->load->view('member/templates/header');
        $this->load->view('v_changePass');
        $this->load->view('member/templates/footer');
    }

    function resetPass()
    {
        // $x['data'] = $this->m_keranjang->get_keranjang();
        $this->load->view('member/templates/header');
        $this->load->view('v_resetPass');
        $this->load->view('member/templates/footer');
    }

    

    function forgotPass()
    {
        // $x['data'] = $this->m_keranjang->get_keranjang();
        $this->load->view('member/templates/header');
        $this->load->view('v_forgotPass');
        $this->load->view('member/templates/footer');
    }

}