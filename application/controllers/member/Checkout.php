<?php
class Checkout extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('m_member');
        $this->load->model('M_Keranjang');
        $this->load->model('m_produk');
        $this->load->library('rajaongkir');
    }
    function index()
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        if ($check_user != NULL) {
            $get_province     = $this->rajaongkir->getProvince();
            $data['users']    = $check_user;
            $cart             = $this->M_Keranjang->get_keranjang_all(array( 'id_member' => $check_user->id_member, 'is_selected' => 1));;
            $data['cart']     = $cart;
            $data['total']    = $this->M_Keranjang->count_selected($check_user->id_member);
            $data['provinces'] = $get_province['rajaongkir']['results'];

            $this->load->view('member/templates/header');
            $this->load->view('member/pages/v_checkout',$data);
            $this->load->view('member/templates/footer');
        } else {
            $this->load->view('v_login');
        }
    }

}