<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

class Location extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('m_member');
        $this->load->library('rajaongkir');
    }
    
    function index()
    {

    }

    function getProvince()
    {   
        $response = $this->rajaongkir->getProvince();
        header('Content-Type: application/json');
		echo json_encode( $response );
    }

}
