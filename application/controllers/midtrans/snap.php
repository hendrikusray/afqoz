<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Snap extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'SB-Mid-server-Tr-wluH4NBzKQTBZ0IgyogSd', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
		$this->load->helper('url');	
		$this->load->model('m_transaction');
		$this->load->model('M_Keranjang');
        $this->load->model('m_member');
        $this->load->model('m_produk');
    }

    public function index()
    {
    	$this->load->view('checkout_snap');
    }

    public function token()
    {
		// Required
		$transaction_details = array(
		  'order_id' => rand(),
		  'gross_amount' => intval($this->input->post('total')), // no decimal allowed for creditcard
		);

		$item_details = array();
		$users	      = $this->input->post('users'); 

		foreach ($this->input->post('cart') as $key) {
			$item_details[] = array(
				'id' 		=> $key['id'],
				'price' 	=> intval($key['total_harga']),
				'quantity'  => 1,
				'name'	    => "{$key['nama_produk']} size {$key['ukuran']}"
			);
		}

		$item_details[] = array (
			'id' 		=> 'a3',
			'price' 	=> intval($this->input->post('ongkir')),
			'quantity'  => 1,
			'name'	    => 'ongkos kirim'
		);


		// Optional
		$item_details = $item_details;

		// Optional
		$billing_address = array(
		  'first_name'    => $users['nama'],
		  'last_name'     => $users['nama'],
		  'address'       => $users['full_address'],
		  'postal_code'   => $users['post_code'],
		  'phone'         => $users['telp'],
		  'country_code'  => 'IDN'
		);

		// Optional
		$shipping_address = array(
		  'first_name'    => "Obet",
		  'last_name'     => "Supriadi",
		  'address'       => $users['full_address'],
		//   'city'          => "Jakarta",
		  'postal_code'   => $users['post_code'],
		  'phone'         => $users['telp'],
		  'country_code'  => 'IDN'
		);

		// Optional
		$customer_details = array(
		  'first_name'    =>  $users['nama'],
		  'email'         => $users['email'],
		  'phone'         => $users['telp'],
		  'billing_address'  => $billing_address,
		  'shipping_address' => $shipping_address
		);

		// Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'days', 
            'duration'  => 1
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );

		error_log(json_encode($transaction_data));
		$snapToken = $this->midtrans->getSnapToken($transaction_data);
		error_log($snapToken);
		echo $snapToken;
    }

    public function finish()
    {

		date_default_timezone_set('Asia/Jakarta');
		$order       = $this->input->post('result');
		$transaction = $this->input->post('transaction');
		$session     = $this->session->all_userdata();
		$users	     = $this->input->post('users'); 
		$check_user  = $this->m_member->member_with_token($session['email'], $session['token']);
		$payment_va  = NULL;
		$produk      = $this->input->post('cart');
		$pembayaran  = NULL;
	
		if(isset($order['va_numbers']) ) {
			$payment_va = $order['va_numbers'][0]['va_number'];
		} else if ($order['payment_type'] == 'cstore') {
			$payment_va = $order['payment_code'];
		} else {
			$payment_va = $order['payment_type'];	
		}
	
		if ($check_user != NULL) {
				$pembayaran = $this->m_transaction->payment(
					array(
						'id_member'   	=>  $check_user->id_member,
						'ongkir'      	=>  intval($this->input->post('ongkir')),
						'url'			=>  $order['pdf_url'],
						'paket_kurir' 	=>  $transaction['package'],
						'service_paket' =>  $transaction['service'],
						'estimasi_paket' => $transaction['estimate'],
						'nama_penerima' =>  $users['nama'],
						'telp'			=>  $users['telp'],
						'total_harga' 	=>  $produk[0]['total_harga'],
						'provinsi'		=>  $transaction['province'],
						'kabupaten'     =>  $transaction['city'],
						'alamat'        => $users['full_address'],
						'no_resi'       => $this->generateRandomString(),
						'kode_pos'		=> $users['post_code'],
						'status_pemesanan' => 0,
						'status_barang' => 'pending',
						'ordercode'     => $order['order_id'],
						'payment_type'  => $order['payment_type'],
						'bank_name'		=> isset($order['va_numbers']) ? $order['va_numbers'][0]['bank'] : $order['payment_type'],
						'va'			=> $payment_va,
					), 'pembayaran'
				);
				foreach ($produk as $key) {
					$order = $this->m_transaction->payment(
						array(
								'id_pembayaran' =>  $pembayaran,
								'qty'		  	=>  intval($key['qty']),
								'harga'       	=>  $key['harga_stok'],
								'ukuran'		=>  $key['ukuran'],
								'nama_produk'	=>  $key['nama_produk'],
						), 'pemesanan'
					);

					$this->m_transaction->payment(
						array(
								'id_order' 	    =>  $order,
								'id_produk'		=>  $key['id_produk'],
						), 'order_produk'
					);

					$this->m_transaction->delete_data(array('id'=> $key['id']),'cart');
				}
		}
		
		// redirect('/member/keranjang/myorder', 'refresh');

    }

	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
