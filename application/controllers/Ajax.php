<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

use Firebase\JWT\JWT;

class Ajax extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('M_Keranjang');
        $this->load->model('m_member');
        $this->load->model('m_produk');
        $this->load->model('m_produk');
        $this->load->library('Rajaongkir');
        $this->load->model('m_transaction');
    }

    function index($arrow = array())
    {
        if (empty($arrow)) {
            $arrow = array(
                'msg' => 'index'
            );
        }

        $this->load->view('member/templates/header');
        $this->load->view('v_resetpass', $arrow);
    }

    function updated($id_produk, $qty, $id)
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        $produk     = $this->M_Keranjang->find(
            array(
                'id_produk' => $id_produk,
                'qty'       => $qty,
                'id'        => $id,
                'id_member' => $check_user->id_member
            )
        );
        header('Content-Type: application/json');
        echo json_encode($produk);
    }

    function unable($id = NULL, $checked = NULL)
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);


        if ($id !== NULL && $checked !== NULL) {
            $this->M_Keranjang->update_data(array(
                'id_member' => $check_user->id_member,
                'id'        => $id
            ), array(
                'is_selected' => $checked
            ), 'cart');
        }

        header('Content-Type: application/json');
        echo json_encode(array('status' => 200, 'msg' => 'success to update'));
    }

    function deletecart($id = NULL, $produk = NULL, $qty = NULL)
    {
        $session    = $this->session->all_userdata();
        $check_user = $this->m_member->member_with_token($session['email'], $session['token']);
        $result     = $this->m_produk->find($produk);

        if ($id !== NULL) {
            $this->M_Keranjang->delete(array(
                'id_member' => $check_user->id_member,
                'id'        => $id
            ));
            $this->m_produk->update_data(
                array(
                    'id_produk'     => $produk
                ),
                array('jumlah_stok' =>  (int)$result->jumlah_stok + (int)$qty),
                'stok'
            );
        }

        header('Content-Type: application/json');
        echo json_encode(array('status' => 200, 'msg' => 'success to update'));
    }

    function getCity($province = NULL)
    {
        if ($province == NULL) {
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'Null paramas province'));
        } else {
            $get_city     = $this->rajaongkir->getCity($province);
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'success', 'data' => $get_city['rajaongkir']['results']));
        }
    }

    function getPaket($to_city = NULL, $courier = NULL)
    {
        if ($to_city == NULL && $courier == NULL) {
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'Null paramas packet and city'));
        } else {
            $payload       = array(
                'origin'        => '419',
                'destination'   => $to_city,
                'weight'        => '100',
                'courier'       => $courier
            );
            $get_paket     = $this->rajaongkir->getCost($payload);

            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'success', 'data' => $get_paket['rajaongkir']['results'][0]['costs']));
        }
    }

    function reportTransaction() {
        $start_date = $this->input->post('start');
        $end_date   = $this->input->post('end');
        if ($start_date == NULL && $end_date == NULL) {
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'NULL params'));
        } else {
            $payload       = array(
               'created_at >=' => $start_date,
               'created_at <=' => $end_date
            );
            $get_data     = $this->m_transaction->report_transaction_function($payload);
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'success', 'data' => $get_data));
        }
    }  
    
    function updateStatusPembayaran($id = null) {
        if ($id != NULL) {
            $data                     = array();
            $data['status_pemesanan'] = '2';
            $data['status_barang']    = 'Sudah Sampai';

            $this->m_transaction->update_data(array('id'=>$id),$data,'pembayaran');
            header('Content-Type: application/json');
            echo json_encode(array('status' => 200, 'message' => 'success', 'data' => 'success'));
        } else {
            header('Content-Type: application/json');
            echo json_encode(array('status' => 500, 'message' => 'failed', 'data' => null));
        }
    }
}
