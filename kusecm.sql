-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2021 at 05:46 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kusecm`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminn`
--

CREATE TABLE `adminn` (
  `id_admin` int(5) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email_admin` varchar(32) NOT NULL,
  `pass_admin` varchar(32) NOT NULL,
  `level` int(2) NOT NULL COMMENT '1. Admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminn`
--

INSERT INTO `adminn` (`id_admin`, `nama_admin`, `username`, `email_admin`, `pass_admin`, `level`) VALUES
(2, 'kus', 'kus', 'kus@g.c', '21232f297a57a5a743894a0e4a801fc3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(5) NOT NULL,
  `judul_artikel` varchar(255) NOT NULL,
  `isi_artikel` text NOT NULL,
  `foto_artikel` varchar(50) NOT NULL,
  `waktu_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `isi_artikel`, `foto_artikel`, `waktu_post`) VALUES
(1, 'BeautyFest Asia 2020 Virtual Red Carpet', 'Konsep virtual bukan jadi alasan kami untuk tampil seadanya. Pada BeautyFest Asia 2020, tim Popbela kompakan bergaya super ekstra layaknya hadir di&nbsp;red carpet&nbsp;sungguhan. Konsep yang telah diusung dari awal tahun, rencananya memang untuk gaya&nbsp;offline red carpet. Namun pandemi membuat kami cukup puas mengunggahnya via sosial media saja.\r\n"Anak-anak" Popbela tampil dengan&nbsp;outfit&nbsp;yang memiliki&nbsp;statement point&nbsp;masing-masing. Bergaya kekinian dengan warna-warni yang&nbsp;playful&nbsp;hingga&nbsp;monochrome look&nbsp;yang glamor.', 'Artikel_2020_11_12_1605172985.jpg', '2020-11-12 09:23:05'),
(3, 'Tips Tampil Modis Secara Instan', 'Siapa bilang tampil modis harus pakai&nbsp;fashion item&nbsp;yang ekstra&nbsp;from head-to-toe. Kamu bisa bergaya keren dengan cara yang instan kok. Yup, menggunakan&nbsp;outfit&nbsp;sehari-hari atau tambahkan beberapa aksesori, penampilanmu akan jauh lebih menarik dari biasanya. Tiru beberapa tips di bawah ini untuk menciptakan gaya modis dengan cara yang mudah.&nbsp;\r\n 1. Daripada dipakai dengan cara yang&nbsp;basic, masukkan bagian depan baju ke dalam celana atau rok. Trik simpel ini langsung membuat OOTD lebih beda! Cosmopolitan.com\r\n 2. Nggak perlu dipakai, cukup sematkan jaket atau&nbsp;blazer&nbsp;pada pundak, akan menciptakan gaya yang keren banget! Cosmopolitan.com\r\n &nbsp; 3. Pilih dua sampai tiga warna yang akan kamu pakai pada OOTD. Tambahkan beberapa aksesori dan&nbsp;you ready to go! Cosmopolitan.com\r\n 4. Andalkan saja&nbsp;matching set&nbsp;untuk tampil&nbsp;chic. Mulai dari&nbsp;pantsuit, skirt suit&nbsp;atau&nbsp;knit set,&nbsp;sesuaikan dengan&nbsp;personal style-mu. Cosmopolitan.com\r\n &nbsp; 5. Beralih ke sepatu, kali ini coba gaya baru yang beda dari', 'Artikel_2020_11_12_1605173174.jpg', '2020-11-12 09:42:54'),
(4, 'Selalu Tampil Elegan, Ini Dress Favorit Kate Middleton Rancangan desainer dunia', 'Sejak resmi menjadi anggota keluarga kerajaan Inggis, Kate Middleton selalu jadi perbincangan dimana-mana. Nggak hanya soal pesonanya yang menawan, namun Kate Middleton juga menginspirasi perempuan dalam berpakaian yang elegan. Yup, gaya modis Kate Middleton sering ditunjukkan dalam berbagai kesempatan, mulai pertemuan antar negara, melakukan aksi sosial hingga di karpet merah.\r\nPunya deretan&nbsp;outfit, dress&nbsp;jadi salah satu&nbsp;fashion item&nbsp;favorit Kate Middleton. Nggak jarang beberapa&nbsp;dress&nbsp;hasil rancangan suatu&nbsp;brand&nbsp;dipakai berulang oleh istri Pangeran William ini.\r\n\r\n&nbsp;\r\n1. Saat sedang mengandung, Kate Middleton sempat menggunakan midi dress bermotif&nbsp;floral&nbsp;rancangan Emilia Wickstead.\r\n\r\n\r\n\r\n2. Hadir di acara pernikahan Pangeran Harry dan Meghan Markle, penampilan Kate Middleton langsung mencuri perhatian dengan&nbsp;dress&nbsp;putih rancangan Alexander McQueen.\r\nMarieclaire.com.au\r\n\r\n\r\n&nbsp;\r\n3. Tertangkap kamera dalam berbagai kesempatan, Kate Middleton sering mengandalkan&nbsp;coat&nbsp;sebagai&nbsp;dress. Salah satunya&nbsp;coat&nbsp;rancangan Max Mara.\r\nMarieclaire.com.au\r\n\r\n\r\n4. Hadir di karpet merah bersama sang suami, Kate Middleton menggunakan gaun perpaduan warna nude dan hitam beraksen&nbsp;lace&nbsp;rancangan Temperley London.\r\nMarieclaire.com.au\r\n\r\n\r\n&nbsp;\r\n5. Kate Middleton tampak manis dalam balutan&nbsp;champagne pink sequin gown&nbsp;rancangan Jenny Packham.\r\nMarieclaire.com.au\r\n\r\n\r\n6. Pada tahun 2011 silam, Kate Middleton sempat bertemu dengan Michelle Obama menggunakan&nbsp;nude dress&nbsp;milik Reiss.\r\nMarieclaire.com.au\r\n', 'Artikel_2020_11_12_1605173281.jpg', '2020-11-12 09:28:01'),
(5, 'Mix N Match Midi Skirt untuk Hijabers Bosen kan pakai celana terus?', 'Selain&nbsp;palazzo pants, midi skirt&nbsp;adalah salah satu&nbsp;fashion item&nbsp;andalan para&nbsp;hijabers. Selain modis, rok juga bisa menutup lekuk tubuh, makanya rok satu ini jadi favorit. Apalagi dengan mengenakan&nbsp;midi skirt,&nbsp;kamu bisa lebih ekspresif ketika&nbsp;mix n match.\r\nBuat kamu yang bosan pakai celana terus, bisa banget lho mencoba gaya&nbsp;mix n match midi skirt&nbsp;untuk&nbsp;hijabers&nbsp;seperti&nbsp;influencer fashion&nbsp;muslimah di bawah ini.\r\n\r\n&nbsp;\r\n1. Boyish Look\r\ninstagram.com/eslimah\r\nInfluencer fashion&nbsp;Eileen Lahi asal Qatar ini mengenakan&nbsp;midi skirt&nbsp;kanvas. Gayanya terlihat sedikit&nbsp;boyish&nbsp;karena Eileen memadu padankan&nbsp;midi skirt&nbsp;dengan&nbsp;culotte trouser&nbsp;dan&nbsp;sneakers.\r\n\r\n\r\n2. Preppy Look\r\ninstagram.com/sorayaulfa15\r\nIngin tampil lebih&nbsp;preppy? Kamu bisa pakai&nbsp;midi skirt&nbsp;tartan&nbsp;dengan&nbsp;legging&nbsp;dan&nbsp;oxford shoes. Supaya tidak terlihat membosankan, kenakan&nbsp;outer statement&nbsp;untuk membuat penampilan lebih berkesan. Misalnya memakai&nbsp;biker jacket.\r\nJika ingin tampil sederhana, kamu bisa contek gaya Soraya Ulfa di atas dengan memakai&nbsp;cardigan&nbsp;panjang.\r\n\r\n\r\n&nbsp;\r\n3. Edgy Look\r\nInstagram.com/siviazizah\r\nKalau soal&nbsp;fashion, Sivia Azizah selalu tampil unik. Nah, kamu bisa memakai&nbsp;midi skirt&nbsp;dan dipadu-padankan dengan&nbsp;sneakers&nbsp;warna senada. Jika ingin tampil lebih&nbsp;edgy&nbsp;dan&nbsp;rebel&nbsp;lagi, kamu bisa padu padankan dengan&nbsp;boots.\r\n\r\n\r\n4. Quirky Look\r\ninstagram.com/tasyakissty\r\nBosan dengan warna ''aman''? Tepat banget kalau kamu mencoba mengenakan&nbsp;midi skirt&nbsp;dengan warna neon atau&nbsp;vibrant.&nbsp;Gaya Tasya Kissty di atas bisa banget kamu contek. Dengan&nbsp;midi skirt layer&nbsp;warna kuning, kamu bisa tampil&nbsp;standout.\r\nApalagi ditambah dengan penggunaan&nbsp;boots.&nbsp;Untuk pakaiannya, kamu bisa pakai&nbsp;sweater crew neck oversize&nbsp;dengan warna yang&nbsp;vibrant.&nbsp;Atau bisa juga pakai jaket bermotif seperti Tasya di atas.\r\n\r\n\r\n&nbsp;\r\n5. Parisian Chic\r\ninstagram.com/ameliaelle\r\nIngin lebih&nbsp;fashionable&nbsp;ala fashionista di Paris? Kamu bisa pakai rok plisket atau&nbsp;pleated skirt&nbsp;milkmu dengan&nbsp;legging.&nbsp;Lalu bisa kenakan&nbsp;pump heels boots, atau&nbsp;ankle boots. Supaya lebih&nbsp;statement, kenakan&nbsp;outer feather jacket&nbsp;seperti gaya Amelia Elle di atas.Mudah kan untuk tampil&nbsp;fashionable&nbsp;dengan&nbsp;mix n match midi skirt&nbsp;di atas? Kuncinya kamu harus percaya diri!\r\n', 'Artikel_2020_11_12_1605173403.jpg', '2020-11-12 09:30:03'),
(6, 'Cara Mudah Membedakan Produk Kulit Asli dan Sintetis', 'Fimela.com, Jakarta&nbsp;Bagi seseorang yang menyukai dunia&nbsp;style&nbsp;dan&nbsp;fashion, pasti akan menggunakan produk berbahan kulit, untuk membuat tambilannya lebih keren dan trendi. Produk yang terbuat dari kulit biasanya akan lebih awet jika digunakan, namun produk yang terbuat dari&nbsp;kulit&nbsp;asli, memiliki harga yang lumayan mahal. Beberapa produk berbahan dasar kulit, terbuat dari kulit yang&nbsp;sintetis, yang dijual dengan harga lebih ekonomis. Agar tidak keliru, berikut cara mudah untuk mengenali produk kulit asli dan sintetis, dilansir dari berbagai sumber: &nbsp; Mencium Bau Produk Cara mudah untuk mengenali perpedaan produk kulit asli, dengan produk kulit yang berbahan sintetis, bisa mencium aroma produk tersebut. Saat pergi ke toko tas atau sepatu, usahakan untuk mencium aroma produk terlebih dahulu, agar tidak keliru membeli barang asli atau sintetis. Perlu diingat, produk yang berbahan dasar kulit asli memiliki aroma kulit asli dari hewan, dan produk berbahan kulit sintetis memiliki aroma seperti plastik saja.\r\nMenyentuh Tekstur Produk\r\nilustrasi cara mudah mengenali produk kulit/unsplash\r\nCara mudah mengenali produk yang berbahan kulit berikutnya, raba atau sentuhlah produk tersebut, sebelum memutuskan untuk membeli produk tersebut. Biasanya produk yang berbahan dasar kulit asli, memiliki tekstus yang sedikit cacat, tergantung dari kulit asli hewan tersebut. Produk berbahan kulit asli, memiliki ciri khas yang menarik, yaitu memiliki tekstur yang sedikit kasar dan ada kerutan atau goresan pada produk. Sedangkan produk yang berbahan dasar kulit sintetis, biasanya memiliki tekstur barang yang mulus, jika terdapat cacat akan terlihat seperti plastik. &nbsp;\r\nMelipat Produk\r\nilustrasi cara mudah mengenali produk kulit/unsplash\r\n&nbsp;Cara mudah mengenali produk yang berbahan kulit asli atau sintetis, bisa dengan menyentuh produknya, dan melipat kulit sedikit. Biasanya produk yang terbuat dari bahan kulit asli, akan mudah meninggalkan bekas dan kerutan, serta berubah warnanya setelah dilipat. Sedangkan produk yang terbuat dari bahan kulit sintetis, akan susah untuk dilipat, karena bahannya kaku dan tidak meninggalkan kerutan. &nbsp;\r\n&nbsp;\r\nMeneteskan Air\r\n&nbsp;ilustrasi cara mudah mengenali produk berbahan kulit/unsplash\r\nCara mudah mengenali produk berbahan kulit sintetis atau asli, bisa dengan menetaskan sedikit air pada produk. Namun jangan dilakukan pada saat di toko. Lakukan tes ini ketika sudah membeli, atau memiliki barang yang berbahan dasar kulit, agar tidak dimarahin penjual pada saat di toko. Biasanya produk yang terbuat dari kulit asli, akan mudah menyerap air, sedangkan produk dari kulit sintetis akan membuat air tersebut menggenang pada produk yang ditetesi air.\r\n&nbsp;\r\nMembakar dengan Api\r\nilustrasi cara mudah mengenali produk kulit/unsplash\r\nCara terakhir untuk mengenali produk yang berbahan dasar kulit, agak sedikit ekstrim, yaitu melakukan tes dengan menggunakan percikan api. Hal ini tentu saja tidak wajib dilakukan, atau tidak bisa dilakukan pada saat di toko. Melakukan tes percikan api harus siap dengan resiko terhadap produk milikmu. Sahabat Fimela bisa menyalakan api menggunakan korek atau lilin, lalu mendekatkan produk berbahan kulit, selama 10 detik saja. Biasanya produk berbahan dasar kulit asli akan sedikit hangus, dan memiliki bau seperti rambut terbakar. Sedangkan kulit sintetis akan langsung terbakar dan memiliki aroma seperti plastik.', 'Artikel_2020_11_12_1605173735.png', '2020-11-12 09:36:44'),
(7, 'STELLARISSA Sambut New Normal dengan Merilis Produk Terbaru dengan Konsep Zero Waste', 'Fimela.com, Jakarta&nbsp;Sejak pandemi dan PSBB, STELLARISSA yakin bahwa setiap orang memiliki perannya masing-masing. Dengan keyakinan inilah STELLARISSA berupaya untuk membantu menangani COVID-19.&nbsp;STELLARISSA&nbsp;pun percaya bahwa fashion pun dapat membantu penanganan COVID-19 ini.\r\nStella pemilik brand STELLARISSA bersama dengan IPMI (Ikatan Perancang Mode Indonesia) berdonasi 5000 masker, 500 surgical grow dan 500 Hamzat. Selain itu&nbsp;STELLARISSA&nbsp;berkeyakinan jika semua dapat melewati pandemi ini.\r\nDengan keyakinan yang tinggi, STELLARISSA merilis produk terbarunya di era new normal ini. Koleksi terbaru dari STELLARISSA ini berpusat pada generasi Z. Menariknya produk terbaru&nbsp;STELLARISSA&nbsp;ini akan dibuat dengan konsep&nbsp;zero waste.', 'Artikel_2020_11_12_1605173925.png', '2020-11-12 09:38:45'),
(8, ' Chanel memperkenalkan koleksi kacamata terbarunya untuk musim gugur 2020', 'Fimela.com, Jakarta&nbsp;Chanel&nbsp;memperkenalkan koleksi kacamata terbarunya untuk musim gugur 2020 yang inspirasinya diambil dari tweed ikonis brand ini sendiri. Koleksi kacamata terbaru dari Chanel ini ingin menonjolkan kontrasnya garis antara feminin dan maskulin.\r\nUntuk koleksi pertama,&nbsp;Chanel&nbsp;mengemboss tweed ke dalam sebuah plakat emas, perak, dan logam berwarna ruthenium untuk menghiasi sisi-sisi bingkai kacamata. Dihadirkan dalam bentuk persegi panjang yang besar, koleksi kacamata ini memiliki beberapa pilihan warna, yaitu hitam, merah anggur, kelabu tua, atau nuansa warna kulit penyu.\r\nAda juga bentuk mata kucing dalam warna hitam, coklat, hijau, atau nuansa warna kulit penyu untuk pilihan yang lebih feminin.&nbsp;Chanel&nbsp;juga menghadirkan bingkai versi optik yang klasik dengan lensa pemblokir cahaya biru', 'Artikel_2020_11_12_1605174046.jpg', '2020-11-12 09:40:46'),
(9, 'Edisi Terbatas Paling Dinanti, Koleksi Kolaborasi Dior x Jordan untuk Dior Fall 2020 Men''s Show', 'Fimela.com, Jakarta&nbsp;Dior&nbsp;membuat kolaborasi menarik dengan Jordan Brand untuk acara Dior Fall 2020 Men''s Show di Miami. Kolaborasi ini menghasilkan sneakers edisi terbatas Air Jordan 1 OG Dior dan koleksi ready to wear, serta aksesori Air Dior.\r\n&nbsp;\r\nSneakers Air Jordan 1 OG Dior dihadirkan dalam 2 pilihan, yaitu versi low top dan high top di mana&nbsp;Dior&nbsp;akan memberikan pengalaman online yang eksklusif untuk mencoba koleksi kolaborasi terbaru ini. Cara pertama, kamu akan diundang untuk mendaftar agar mendapatkan kesempatan membeli sepasang sneakers kolaborasi antara Dior dan Jordan Brand ini, memilih versi sneakers yang diinginkan, butik yang dituju, lokasi pengambilan barang, dan ukuran sepatu.\r\nSetiap orang hanya memiliki kesempatan untuk 1 kali pendaftaran, memilih 1 butik, pilihan versi sneakers yang diinginkan, dan ukuran. Sneakers Air Jordan 1 OG Dior hanya akan tersedia secara eksklusif di beberapa butik&nbsp;Dior&nbsp;yang terpilih, sehingga konsepnya adalah&nbsp;first come, first served.\r\n&nbsp;', 'Artikel_2020_11_12_1605174101.jpg', '2020-11-12 09:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `harga` double NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` double NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `is_selected` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Sepatu'),
(2, 'Celana'),
(25, 'Tas Gunung'),
(27, 'Tas Gunung'),
(28, 'Baju Baru Nie'),
(29, 'Sepatu Sport'),
(30, 'Tas Laptop'),
(31, 'Tshirt Vertical'),
(32, 'Tas'),
(33, 'Jaket'),
(34, 'Sepatu Sekolah'),
(35, 'Hoodie Sweater'),
(36, 'Hoodie Jaket');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(5) NOT NULL,
  `nama_member` varchar(50) NOT NULL,
  `username_member` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password_member` varchar(32) NOT NULL,
  `token` text NOT NULL,
  `telp` varchar(14) NOT NULL,
  `provinsi` varchar(25) NOT NULL,
  `kabupaten` varchar(25) NOT NULL,
  `kecamatan` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama_member`, `username_member`, `email`, `password_member`, `token`, `telp`, `provinsi`, `kabupaten`, `kecamatan`, `alamat`, `kode_pos`, `status`, `created_at`) VALUES
(14, 'hendrikus adi purnama', 'hendrikusray', 'hendrikusray@gmail.com', '202cb962ac59075b964b07152d234b70', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJlbWFpbCI6ImhlbmRyaWt1c3JheUBnbWFpbC5jb20ifQ.KtATmhmB9C5XnGWt5cRRCUWl85vcS36vgW6TnTmEO4iWlfJk5q7T9GPHeUEAR7QFMp_0t89cXUR_M1G8rEjlAKYQ5hybZvr3ftpI_b5ufNBqZG85txC-ntQTp99QKvegmN9_7Umn4apJ3Z_ZchrCCMWTZuTnQvRm_Elcouka7io', '12', '', '', '', 'jambi ku', 0, 1, '2021-06-12 22:40:09'),
(15, 'hendrikus adi purnama', 'jancuk', 'durubokirr-7125@yopmail.com', '57f842286171094855e51fc3a541c1e2', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJlbWFpbCI6ImR1cnVib2tpcnItNzEyNUB5b3BtYWlsLmNvbSJ9.ff4ZxZMl1OMGILIoh93AeeRNnUgysGi4PkX7lQGqK5hp2_33OqN5eHxNVJ7dpGER3ltBHdPDOSGE_v4SF7SGf_pp-2uA15SXA_YLSb7BNQY-rh-MF4Oh7t6yyw3BIuOaUbSRir4YTvWsKVFqo6EN6YZHPrGZxWxBEzsM5CIZlPo', '', '', '', '', 'halo', 0, 0, '2021-06-12 22:40:09'),
(21, 'hendrikus adi purnama', '12', 'afqoz.ecommerce@gmail.com', '202cb962ac59075b964b07152d234b70', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJlbWFpbCI6ImFmcW96LmVjb21tZXJjZUBnbWFpbC5jb20ifQ.HouRmBmMeTENk-3TdIPMbVop-JQuHTVVFoMGJ6na3t0Bowht4akgHlFj1yJwO51sOW1lG926IeBxzwHCt8Xizxw7uogOqrMant0_KdEYKbh_nYbEzwX2x84_-63h2Mx_c-1kd0f7mLUqdZgvrwcDYIFyQQZvljw6dhunOreaJ1U', '', '', '', '', '123', 0, 1, '2021-06-12 22:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `order_produk`
--

CREATE TABLE `order_produk` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_produk`
--

INSERT INTO `order_produk` (`id`, `id_order`, `id_produk`) VALUES
(15, 25, 40),
(17, 27, 40),
(18, 28, 40),
(22, 32, 40);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(5) NOT NULL,
  `id_member` int(5) NOT NULL,
  `url` text NOT NULL,
  `va` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `ordercode` text NOT NULL,
  `status_pemesanan` int(11) NOT NULL,
  `status_barang` varchar(255) NOT NULL,
  `ongkir` double NOT NULL,
  `total_harga` double NOT NULL,
  `paket_kurir` varchar(255) NOT NULL,
  `service_paket` varchar(255) NOT NULL,
  `estimasi_paket` varchar(255) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `telp` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kabupaten` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_resi` varchar(255) NOT NULL,
  `kode_pos` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_member`, `url`, `va`, `bank_name`, `payment_type`, `ordercode`, `status_pemesanan`, `status_barang`, `ongkir`, `total_harga`, `paket_kurir`, `service_paket`, `estimasi_paket`, `nama_penerima`, `telp`, `provinsi`, `kabupaten`, `alamat`, `no_resi`, `kode_pos`, `created_at`) VALUES
(13, 14, 'https://app.sandbox.midtrans.com/snap/v1/transactions/6acf237c-b0e4-4951-9d43-732ee5408ec6/pdf', '49168392334', 'bca', 'bank_transfer', '26475', 0, 'expire', 8000, 46000, 'jne', 'OKE', '3-4', 'hendrikus adi purnama', '12', 'DI Yogyakarta', 'Gunung', 'jambi ku', 'CVUofDJvJN', '0', '2021-05-22 16:11:38'),
(14, 14, 'https://app.sandbox.midtrans.com/snap/v1/transactions/22a63e59-359a-412e-a8a4-47ca436a17ab/pdf', '49168279029', 'bca', 'bank_transfer', '22802', 0, 'expire', 19000, 17000, 'jne', 'OKE', '6-7', 'hendrikus adi purnama', '12', 'Banten', 'Pandeglang', 'jambi ku', '60dM8TEM1X', '0', '2021-05-22 16:09:18'),
(15, 14, 'https://app.sandbox.midtrans.com/snap/v1/transactions/3974bc3f-6b8b-4ee8-9ea4-06f3130bd30c/pdf', '49168893695', 'bca', 'bank_transfer', '21364', 1, 'settlement', 19000, 24000, 'jne', 'OKE', '6-7', 'agustina heny', '086321312', 'Banten', 'Lebak', 'jambi kota aneh', '8cHocpfeqr', '111212', '2021-05-22 16:12:12'),
(19, 14, 'https://app.sandbox.midtrans.com/snap/v1/transactions/f11faf4a-3f42-4b65-a0d3-a20bddb0a55e/pdf', '49168300643', 'bca', 'bank_transfer', '17106', 1, 'Dikirim', 46000, 12000, 'jne', 'OKE', '7-8', 'hendrikus adi purnama', '12', 'Bengkulu', 'Bengkulu', 'jambi ku', 'qPcBfANM7o', '0', '2021-05-22 17:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_pembayaran`, `qty`, `harga`, `ukuran`, `nama_produk`, `id_produk`) VALUES
(25, 13, 3, 17000, 'M', 'sepatu', 0),
(27, 14, 1, 17000, 'M', 'sepatu', 0),
(28, 15, 2, 12000, 'L', 'sepatu', 0),
(32, 19, 1, 12000, 'L', 'sepatu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(5) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `foto_produk` longtext NOT NULL,
  `diskon` int(32) NOT NULL,
  `deskripsi` text NOT NULL,
  `berat_produk` int(5) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `id_kategori`, `foto_produk`, `diskon`, `deskripsi`, `berat_produk`, `status`, `created_at`) VALUES
(40, 'sepatu', 1, '["Produk_2021_01_18_1610983630.png","Produk_2021_01_18_16109836301.png","Produk_2021_01_18_16109836302.png"]', 3, 'dhfsdfjsdkfj dsfsjhasdzxc', 4, 0, '2021-06-12 22:34:46'),
(41, 'sdfds', 25, '["Produk_2021_01_11_1610360535.jpg","Produk_2021_01_11_16103605351.jpg","Produk_2021_01_11_16103605352.jpg"]', 0, 'dhfsdfjsdkfj dsfsjh', 25, 0, '2021-06-12 22:34:46'),
(42, 'Denimzzp', 1, '["Produk_2021_01_18_1610983716.png"]', 0, 'dfdf fdfdgfd dfgfg fgewf2', 25, 0, '2021-06-12 22:34:46'),
(43, 'Baju  Zara Hitam', 28, '["Produk_2021_01_13_1610520925.png","Produk_2021_01_13_1610520928.jpg","Produk_2021_01_13_16105209281.jpg"]', 0, 'Baju mmerk Zara new edition. Ready ukuran: S -> 50	CM X 47 CM M -> 52	CM X 50 CM L -> 54	CM X 52 CM XL -> 58	 CM X 54 CM', 28, 1, '2021-06-12 22:34:46'),
(44, 'sds', 28, '["Produk_2021_01_18_1610988123.png","Produk_2021_01_18_16109881231.png"]', 23, 'ad', 28, 0, '2021-06-12 22:34:46'),
(45, '1', 1, '["Produk_2021_04_27_1619498445.jpg"]', 1, '1', 1, 0, '2021-06-12 22:34:46'),
(46, '1', 1, '["Produk_2021_05_11_1620737243.jpg"]', 1, 'tes', 1, 0, '2021-06-12 22:34:46'),
(47, 'q', 2, '["Produk_2021_05_17_1621236361.jpg"]', 1, '1', 2, 0, '2021-06-12 22:34:46'),
(48, 'tes 1', 1, '["Produk_2021_05_20_1621522957.jpg"]', 1, '1', 2, 0, '2021-06-12 22:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(5) NOT NULL,
  `id_produk` int(5) NOT NULL,
  `ukuran_stok` varchar(5) NOT NULL,
  `jumlah_stok` varchar(5) NOT NULL,
  `harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id_stok`, `id_produk`, `ukuran_stok`, `jumlah_stok`, `harga`) VALUES
(2, 40, 'L', '29', 12000),
(3, 40, 'M', '32', 17000),
(4, 42, 'S', '1', 50000),
(5, 41, 'L', '7', 0),
(6, 44, 'L', '2', 0),
(99, 43, 'M', '1', 0),
(100, 48, 'K', '1', 120000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminn`
--
ALTER TABLE `adminn`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produk_cart` (`id_produk`),
  ADD KEY `fk_produk_member` (`id_member`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `order_produk`
--
ALTER TABLE `order_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order` (`id_order`),
  ADD KEY `fk_id_produk` (`id_produk`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`),
  ADD KEY `fk_id_pembayaran` (`id_pembayaran`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_kategori_2` (`id_kategori`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`),
  ADD KEY `id_produk` (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminn`
--
ALTER TABLE `adminn`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `order_produk`
--
ALTER TABLE `order_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `fk_produk_cart` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_produk_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_produk`
--
ALTER TABLE `order_produk`
  ADD CONSTRAINT `fk_id_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `fk_order` FOREIGN KEY (`id_order`) REFERENCES `pemesanan` (`id_pemesanan`);

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_2` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`);

--
-- Constraints for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD CONSTRAINT `fk_id_pembayaran` FOREIGN KEY (`id_pembayaran`) REFERENCES `pembayaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
